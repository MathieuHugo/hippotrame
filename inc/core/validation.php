<?php


function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[random_int(0, $charactersLength - 1)];
    }
    return $randomString;
}
function cleanXss($key)
{
    return trim(strip_tags($_POST[$key]));
}

function validmail($errors, $valeur, $key)
{
    if (empty($valeur)) {
        $errors[$key] = 'Veuillez renseigner un email';
    } elseif (!filter_var($valeur, FILTER_VALIDATE_EMAIL)) {
        $errors[$key] = 'Veuillez renseigner un email valide';
    }
    return $errors;
}
function viewError($err, $key)
{
    if(!empty($err[$key])) {
        echo $err[$key];
    }
}
function validText($err, $value,$keyErr,$min,$max)
{
    if(!empty($value)) {
        if(mb_strlen($value) < $min ) {
            $err[$keyErr] = 'Veuillez renseigner au moins '.$min.' caractères';
        } elseif(mb_strlen($value) > $max ) {
            $err[$keyErr] = 'Veuillez renseigner pas plus de '.$max.' caractères';
        }
    } else {
        $err[$keyErr] = 'Veuillez renseigner ce champ';
    }
    return $err;
}

function isLogged()
{
    if (!empty($_SESSION['user']['id'])) {
        if (!empty($_SESSION['user']['nom'])) {
            if (!empty($_SESSION['user']['prenom'])) {
                if (!empty($_SESSION['user']['mail'])) {
                    if (!empty($_SESSION['user']['status'])) {
                        if (!empty($_SESSION['user']['created_at'])) {
                            return true;
                        }
                    }
                }
            }
        }
    }
    return false;
}

function isAdmin(){
    if(isLogged()){
        if($_SESSION['user']['status'] == 'admin'){
            return true;
        }
    }
    return false;
}