<?php

/////////////////
//utilisateurs//
///////////////
function getAllUser(){
    global $pdo;
    $sql = "SELECT * FROM `user`";
    $query = $pdo->prepare($sql);
    $query->execute();
    return $query->fetchAll();
}
function updateUser($nom,$prenom,$mail,$status,$id){
    global $pdo;
    $sql= "UPDATE `user` SET `nom`= :nom,`prenom`= :prenom,`mail`= :mail,`modified_at`= NOW(),`status`= :status WHERE `id` = :id";
    $query=$pdo->prepare($sql);
    $query->bindValue('nom', $nom, PDO::PARAM_STR);
    $query->bindValue('prenom', $prenom, PDO::PARAM_STR);
    $query->bindValue('mail', $mail, PDO::PARAM_STR);
    $query->bindValue('status', $status, PDO::PARAM_STR);
    $query->bindValue('id', $id, PDO::PARAM_STR);
    $query->execute();
}
/////////////
//Articles//
///////////
function getAllArticles(){
    global $pdo;
    $sql = "SELECT * FROM `blog`";
    $query = $pdo->prepare($sql);
    $query->execute();
    return $query->fetchAll();
}
function insertArticle($titre,$auteur,$contenu,$extrait,$image,$status){
    global $pdo;
    $sql= "INSERT INTO `blog`(`titre`, `auteur`, `content`, `extrait`, `image`, `created_at`,  `status`) 
    VALUES ( :titre, :auteur, :contenu, :extrait, :image, NOW(), :status)";
    $query=$pdo->prepare($sql);
    $query->bindValue('titre', $titre, PDO::PARAM_STR);
    $query->bindValue('auteur', $auteur, PDO::PARAM_STR);
    $query->bindValue('contenu', $contenu, PDO::PARAM_STR);
    $query->bindValue('extrait', $extrait, PDO::PARAM_STR);
    $query->bindValue('image', $image, PDO::PARAM_STR);
    $query->bindValue('status', $status, PDO::PARAM_STR);
    $query->execute();
}
function updateArticles($titre,$auteur,$contenu,$extrait,$image,$status,$id){
    global $pdo;
    $sql= "UPDATE `blog` SET `titre`= :titre,`auteur`= :auteur,`content`= :contenu,`extrait`= :extrait ,`image`= :image,`modified_at`= NOW(),`status`= :status WHERE `id` = :id";
    $query=$pdo->prepare($sql);
    $query->bindValue('titre', $titre, PDO::PARAM_STR);
    $query->bindValue('auteur', $auteur, PDO::PARAM_STR);
    $query->bindValue('contenu', $contenu, PDO::PARAM_STR);
    $query->bindValue('extrait', $extrait, PDO::PARAM_STR);
    $query->bindValue('image', $image, PDO::PARAM_STR);
    $query->bindValue('status', $status, PDO::PARAM_STR);
    $query->bindValue('id', $id, PDO::PARAM_STR);
    $query->execute();
}


//////////
// Avis//
////////
function getAllAvis(){
    global $pdo;
    $sql = "SELECT * FROM `avis`";
    $query = $pdo->prepare($sql);
    $query->execute();
    return $query->fetchAll();
}
function insertAvis($nom,$prenom,$job,$titre,$message,$etat){
    global $pdo;
    $sql = "INSERT INTO `avis`( `nom`, `prenom`, `title`,`job` ,`message`,`created_at`,`etat`) 
                VALUES (:nom, :prenom , :title , :job ,:message ,NOW() , :etat )";
    $query=$pdo->prepare($sql);
    $query->bindValue('nom', $nom, PDO::PARAM_STR);
    $query->bindValue('prenom', $prenom, PDO::PARAM_STR);
    $query->bindValue('title', $titre, PDO::PARAM_STR);
    $query->bindValue('job', $job, PDO::PARAM_STR);
    $query->bindValue('message', $message, PDO::PARAM_STR);
    $query->bindValue('etat', $etat, PDO::PARAM_STR);
    $query->execute();
}
function updateAvis($nom,$prenom,$job,$titre,$message,$etat,$id){
    global $pdo;
    $sql = "UPDATE `avis` SET `nom`= :nom ,`prenom`= :prenom ,`title`= :titre ,`job`= :job ,`message`= :message,`modified_at`=NOW(),`etat`= :etat WHERE `id`= :id";
    $query = $pdo->prepare($sql);
    $query->bindValue('nom', $nom, PDO::PARAM_STR);
    $query->bindValue('prenom', $prenom, PDO::PARAM_STR);
    $query->bindValue('job', $job, PDO::PARAM_STR);
    $query->bindValue('titre', $titre, PDO::PARAM_STR);
    $query->bindValue('message', $message, PDO::PARAM_STR);
    $query->bindValue('etat', $etat, PDO::PARAM_STR);
    $query->bindValue('id', $id, PDO::PARAM_STR);
    $query->execute();
}