# Guide d'installation d'Hippotrame

Cette documentation explique les étapes nécessaires pour installer et exécuter l'application web Hippotrame sur votre propre environnement.

## Clonage du dépôt

```bash
git clone https://gitlab.com/MathieuHugo/hippotrame.git
cd weblipack
```

## Installation de la dépendance

```bash
npm install
```

## Utilisation de Webpack

Pour surveiller les changements et recompiler automatiquement :
```bash
npm run watch
```

Pour construire une version optimisée pour la production :
```bash
npm run build
```

## Exécution du serveur PHP

Pour exécuter le serveur PHP et afficher le site, utilisez la commande suivante :

```bash
php -S localhost:2323 -t public
```