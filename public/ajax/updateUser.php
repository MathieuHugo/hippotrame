<?php
require('../../inc/core/pdo.php');
require('../../inc/function.php');

$errors = array();

// Faille XSS
$nom = cleanXss('nom');
$prenom = cleanXss('prenom');
$mail=cleanXss('mail');
$status=cleanXss('status');
$id=cleanXss('id');
// format valeur en BDD
$nom=strtoupper($nom);
$prenom=ucfirst($prenom);

// Validations
$errors = validText($errors, $nom, 'nom', 2, 50,);
$errors = validText($errors, $prenom, 'prenom', 2, 50,);
$errors = validmail($errors, $mail, 'mail');

if (count($errors) == 0) {
    updateUser($nom,$prenom,$mail,$status,$id);
}
showJson(
    array(
        'errors' => $errors,
        'nom' => $nom,
        'prenom' => $prenom,
        'mail' => $mail,
        'status' => $status,
        'id'=> $id
    )
);
