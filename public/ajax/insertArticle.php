<?php
require('../../inc/core/pdo.php');
require('../../inc/function.php');

$errors = array();

// Faille XSS
$image = cleanXss('image');
$titre = cleanXss('titre');
$extrait = cleanXss('extrait');
$contenu=cleanXss('contenu');
$auteur=cleanXss('auteur');
$status=cleanXss('status');

// format valeur en BDD
$titre=ucfirst($titre);
$extrait=ucfirst($extrait);
$contenu=ucfirst($contenu);
$auteur=ucfirst($auteur);
// Validations
$errors = validText($errors, $titre, 'titre', 2, 100,);
$errors = validText($errors, $extrait, 'extrait', 2, 200,);
$errors = validText($errors, $contenu, 'contenu',2,10000);
$errors = validText($errors, $auteur, 'auteur',2,50);

$uploadDir = '../asset/media/';

if ($_FILES['upload']['error'] === UPLOAD_ERR_OK) {
    $tmpFilePath = $_FILES['upload']['tmp_name']; // Chemin temporaire du fichier téléchargé
    $newFilePath = $uploadDir . $_FILES['upload']['name']; // Chemin de destination final

    if (move_uploaded_file($tmpFilePath, $newFilePath)) {

    } else {
        echo json_encode(array('errors' => 'Une erreur est survenue lors du téléchargement du fichier.'));
    }
} else {
    echo json_encode(array('errors' => 'Erreur lors du téléchargement du fichier.'));
}
if(count($errors)==0){
    insertArticle($titre,$auteur,$contenu,$extrait,$image,$status);
}

showJson(
    array(
        'errors' => $errors,
        'image' => $image,
        'titre' => $titre,
        'extrait' => $extrait,
        'contenu' => $contenu,
        'auteur' => $auteur,
        'status' => $status,
    )
);