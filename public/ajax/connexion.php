<?php
require('../../inc/function.php');



$mail = '';

$success = false;
$errors = array();

// Faille XSS
$mail = cleanXss('mail');
$password = cleanXss('password');

$errors = validmail($errors, $mail, 'mail');

// Validation
$sql = "SELECT * FROM user
            WHERE mail = :mail";
$query = $pdo->prepare($sql);
$query->bindValue('mail', $mail, PDO::PARAM_STR);
$query->execute();
$user = $query->fetch();

if (empty($password)) {
    $errors['login'] = 'Veuillez renseigner votre mot de passe.';
}


if (!empty($user)) {
    if (password_verify($password, $user['password'])) {
        $_SESSION['user'] = array(
            'id'     => $user['id'],
            'nom' => $user['nom'],
            'prenom' => $user['prenom'],
            'mail'  => $user['mail'],
            'status'   => $user['status'],
            'created_at'   => $user['created_at'],

        );
        $success = true;
    } else {
        $errors['login'] = 'Errors credentials';
    }
}




showJson(
    array(
        'errors' => $errors,
        'success' => $success
    )

);
