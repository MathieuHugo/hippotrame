<?php
require('../../inc/core/pdo.php');
require('../../inc/function.php');

$errors = array();

// Faille XSS
$nom = cleanXss('nom');
$prenom = cleanXss('prenom');
$job = cleanXss('job');
$titre=cleanXss('titre');
$message=cleanXss('message');
$etat=cleanXss('etat');
// format valeur en BDD
$nom=strtoupper($nom);
$prenom=ucfirst($prenom);
$job=ucfirst($job);
$titre=ucfirst($titre);
$message=ucfirst($message);
// Validations
$errors = validText($errors, $nom, 'nom', 2, 30,);
$errors = validText($errors, $prenom, 'prenom', 2, 30,);
$errors = validText($errors, $job, 'job',2,30);
$errors = validText($errors, $titre, 'titre',2,30);
$errors = validText($errors, $message, 'message',2,120);

if (count($errors) == 0) {
    insertAvis($nom,$prenom,$job,$titre,$message,$etat);

}else{
    die('404');
}
showJson(
    array(
        'errors' => $errors,
        'nom' => $nom,
        'prenom' => $prenom,
        'job' => $job,
        'titre' => $titre,
        'message' => $message,
        'etat' => $etat,
    )
);
