<?php
require('../../inc/function.php');


$errors = array();
$success = false;



// Faille XSS
$nom = cleanXss('nom');
$prenom = cleanXss('prenom');
$mail = cleanXss('mail');
$password = cleanXss('password');
$password2 = cleanXss('password2');
// Validations

$errors = validmail($errors, $mail, 'mail');
$errors = validText($errors, $nom, 'nom', 3, 50,);
$errors = validText($errors, $prenom, 'prenom', 3, 50,);

// unique email
if (empty($errors['mail'])) {
    $sql = "SELECT id FROM user WHERE mail = :mail";
    $query = $pdo->prepare($sql);
    $query->bindValue('mail', $mail);
    $query->execute();
    $verifEmail = $query->fetch();
    if (!empty($verifEmail)) {
        $errors['mail'] = 'E-mail deja pris';
    }
}
// Validation des deux passwords
if (!empty($password) && !empty($password2)) {
    if ($password != $password2) {
        $errors['password'] = 'Vos mots de passe sont différents.';
    } elseif (mb_strlen($password) < 6) {
        $errors['password'] = 'Votre mot de passe est trop court (6 caractères minimum)';
    }
} else {
    $errors['password'] = 'Veuillez renseigner les mots de passe.';
}



if (count($errors) == 0) {
    $hashPassword = password_hash($password, PASSWORD_DEFAULT);
    $status = 'user';
    $token = generateRandomString(130);
    $sql = "INSERT INTO user (nom, prenom, mail, password, created_at, status, token)
                VALUES (:nom, :prenom , :mail, :pass, NOW(), '$status', '$token')";
    $query = $pdo->prepare($sql);
    $query->bindValue('nom', $nom, PDO::PARAM_STR);
    $query->bindValue('prenom', $prenom, PDO::PARAM_STR);
    $query->bindValue('mail',   $mail, PDO::PARAM_STR);
    $query->bindValue('pass',   $hashPassword, PDO::PARAM_STR);
    $query->execute();

    $success = true;
}



showJson(
    array(
        'errors' => $errors,
        'success' => $success
    )

);
