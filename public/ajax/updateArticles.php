<?php
require('../../inc/core/pdo.php');
require('../../inc/function.php');

$errors = array();

// Faille XSS
$image = cleanXss('image');
$titre = cleanXss('titre');
$extrait = cleanXss('extrait');
$contenu=cleanXss('contenu');
$auteur=cleanXss('auteur');
$status=cleanXss('status');;
$id=cleanXss('id');
// format valeur en BDD
$titre=ucfirst($titre);
$extrait=ucfirst($extrait);
$contenu=ucfirst($contenu);
$auteur=ucfirst($auteur);
// Validations
$errors = validText($errors, $titre, 'titre', 2, 100,);
$errors = validText($errors, $extrait, 'extrait', 2, 200,);
$errors = validText($errors, $contenu, 'contenu',2,10000);
$errors = validText($errors, $auteur, 'auteur',2,50);

$uploadDir ='../asset/media/';
if(!empty($HTTP_POST_FILES)){
    if($_FILES['upload']['error'] === UPLOAD_ERR_OK) {
        $tmpFilePath = $_FILES['upload']['tmp_name']; // Chemin temporaire du fichier téléchargé
        $newFilePath = $uploadDir . $_FILES['upload']['name']; // Chemin de destination final

        if(move_uploaded_file($tmpFilePath, $newFilePath)) {
            // Le fichier a été déplacé avec succès vers le dossier souhaité
            // Vous pouvez maintenant poursuivre votre traitement, par exemple, sauvegarder le chemin dans une base de données
        }else {
            // Gestion des erreurs si le déplacement du fichier a échoué
            echo json_encode(array('errors' => 'Une erreur est survenue lors du téléchargement du fichier.'));
        }
    }else {
        // Gestion des autres erreurs de téléchargement
        echo json_encode(array('errors' => 'Erreur lors du téléchargement du fichier.'));
    }
}
if(count($errors)==0){
    updateArticles($titre,$auteur,$contenu,$extrait,$image,$status,$id);
}

showJson(
    array(
        'errors' => $errors,
    )
);