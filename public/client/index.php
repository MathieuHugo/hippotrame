<?php

require('../../inc/function.php');

include('../../view/utilisateurs/header.php');
?>


    <!-- Début : Contenu de la page -->
    <div class="container-fluid">

        <!-- Accueil -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Analyse réseau</h1>
        </div>

        <!-- Cartes avec données -->
        <div class="row">

            <!-- Carte : TTL expirés (Total) -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2 ttl-border">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1 ttl-title">
                                    TTL expirés (Total)</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800 ttl-result"></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-heart-pulse text-gray-300 ttl-icone"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Carte : Membre premium -->
            <div class="col-xl-3 col-md-6 mb-4">
                <a href="" class="premium-activation premium-link">
                    <div class="card border-left-success shadow h-100 py-2 premium-background premium-border">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1 premium-title">
                                        Devenez un membre</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800 premium-text">PREMIUM</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fa-solid fa-hippo text-gray-300 hippo-icone"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <!-- Graphiques (chart.js) -->

        <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div
                            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary hippo-text">Nombre de trames par type de requête</h6>
                        <div class="dropdown no-arrow">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                                 aria-labelledby="dropdownMenuLink">
                                <div class="dropdown-header">Dropdown Header:</div>
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div class="chart-area">
                            <canvas id="myAreaChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div
                            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary hippo-text">Statut des requêtes</h6>
                        <div class="dropdown no-arrow">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                                 aria-labelledby="dropdownMenuLink">
                                <div class="dropdown-header">Dropdown Header:</div>
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div class="chart-pie pt-4 pb-2">
                            <canvas id="myPieChart"></canvas>
                        </div>
                        <div class="mt-4 text-center small">
                                        <span class="mr-2">
                                            <i class="fas fa-circle text-primary color-succes"></i> Succès
                                        </span>
                            <span class="mr-2">
                                            <i class="fas fa-circle text-success color-failed"></i> Échec
                                        </span>
                            <span class="mr-2">
                                            <i class="fas fa-circle text-info color-wait"></i> En attente
                                        </span>
                            <span class="mr-2">
                                            <i class="fas fa-circle text-info color-unknow"></i> Inconnu
                                        </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid premium-none">

            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary hippo-text">Tableau des logs (version bêta)</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                            </tr>
                            </tfoot>
                            <tbody>
                            <tr>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

        <!-- Content Row -->
        <div class="row">

            <!-- Content Column -->
            <div class="col-lg-6 mb-4">

                <!-- Illustrations -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary hippo-text">L'application Hippotrame (bientôt)</h6>
                    </div>
                    <div class="card-body">
                        <div class="text-center">
                            <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;"
                                 src="img/undraw_application.svg" alt="...">
                        </div>
                        <p>Découvrez l'avenir de l'analyse réseau avec <a
                                    target="_blank" rel="nofollow" href="">Hippotrame</a> ! Bientôt disponible sur téléphone, notre nouvelle appli révolutionnera
                            la surveillance réseau. Restez connecté et toujours sécurisé avec Hippotrame !</p>
                        <a target="_blank" rel="nofollow" href="">En savoir plus &rarr;</a>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 mb-4">

                <!-- Approach -->
                <div class="card shadow mb-4 litzer-box">
                    <div class="card-header py-3 litzer-header">
                        <h6 class="m-0 font-weight-bold text-primary litzer-text">Notre partenaire : <span class="litzer-color">Weblitzer</span></h6>
                    </div>
                    <div class="card-body">
                        <p>Plongez dans l'univers numérique d'Antoine Quidel, un développeur web freelance passionné.
                            Sur Weblitzer, l'art de coder prend vie. Explorez un portfolio riche en créativité,
                            où chaque ligne de code raconte une histoire unique. Avec expertise et savoir-faire,
                            Antoine transforme vos idées en expériences web exceptionnelles.</p>
                        <p class="mb-0">Bienvenue sur Weblitzer, où la magie d'internet rencontre l'excellence du développement web.</p>
                        <div class="litzer-img">
                            <a target="_blank" rel="nofollow" href="https://www.weblitzer.fr/"> <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="img/weblitzer.svg" alt="..."></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>



    </div>
    <!-- End of Main Content -->

<?php
include('../../view/utilisateurs/footer.php');
