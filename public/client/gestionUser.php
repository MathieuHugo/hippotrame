<?php
require('../../inc/function.php');

if (!isAdmin()) {
    header("Location: ../index.php");
    exit;
}


$titre = "Gestion Utilisateurs";

include('../../view/utilisateurs/header.php');

?>
<section id="gestionUsers">

</section>
<?php
include('../../view/utilisateurs/footer.php');
