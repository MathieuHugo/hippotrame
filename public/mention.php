<?php
require('../inc/function.php');

include('../view/header.php'); ?>

    <section id="mention">
        <div class="wrap">
            <div class="mentionTitle">
                <h1>Mentions légales</h1>
                <p>Dernière mise à jour : 14/12/2023</p>
            </div>
            <div class="mentionBox">
                <h2>Identification du site</h2>
                <p>Nom du site : Hippotrame<br>
                    Adresse du site web : <a href="http://www.hippotrame.fr">www.hippotrame.fr</a><br>
                    Adresse physique de l'entreprise : 437 Rue Hippo, Trame-Ville, Réseau-Land<br>
                    Numéro de téléphone : 01 23 45 67 89<br>
                    Adresse e-mail : <a href="mailto:contact@hippo-net.com">contact@hippo-net.com</a></p>

                <h2>Éditeur du site</h2>
                <p>Raison sociale de l'entreprise : Hippo-Net Ltd.<br>
                    Forme juridique : Société par actions simplifiée<br>
                    Capital social : 300 000 euros<br>
                    Numéro d'inscription au registre du commerce : RC123456789<br>
                    Adresse du siège social : 437 Rue Hippo, Trame-Ville, Réseau-Land</p>

                <h2>Directeur de la publication</h2>
                <p>Nom du responsable du contenu du site : Dr. A. Imaginaire<br>
                    Coordonnées du responsable : 437 Rue Hippo, Trame-Ville, Réseau-Land |
                    <a href="mailto:contact@hippo-net.com">contact@hippo-net.com</a> | 01 23 45 67 89</p>

                <h2>Hébergeur</h2>
                <p>Nom de la société d'hébergement : FireWall Corp.<br>
                    Adresse de l'hébergeur : 456 Avenue Sécurité, Antivirus, Pays-Protégé<br>
                    Numéro de téléphone de l'hébergeur : 09 32 54 76 98</p>

                <h2>Finalité du site</h2>
                <p>L'objectif d'Hippotrame est de fournir à l'utilisateur l'analyse en toute sécurité de leur
                    trame réseau.</p>

                <h2>Consentement explicite</h2>
                <p>En utilisant le service d'Hippotrame, l'utilisateur donne son consentement
                    explicite pour la collecte et le traitement de ses données aux fins spécifiées.</p>

                <h2>Sécurité des données</h2>
                <p>Hippotrame met en place des mesures de sécurité robustes pour assurer la protection
                    des données de ses utilisateurs.</p>

                <h2>Durée de conservation des données</h2>
                <p>Les données seront conservées pendant une période de 30 jours,
                    après laquelle elles seront traitées conformément à notre politique de confidentialité.</p>

                <h2>Droit d'accès et de rectification</h2>
                <p>Les utilisateurs ont le droit d'accéder à leurs données
                    et de les rectifier en cas d'inexactitude.</p>

                <h2>Responsabilité</h2>
                <p>Hippotrame ne peut garantir l'exactitude totale des informations contenues
                    dans l'analyse de la trame réseau et décline toute responsabilité quant à l'utilisation
                    des informations par les utilisateurs.</p>

                <h2>Liens externes</h2>
                <p>Hippotrame décline toute responsabilité quant au contenu des sites externes
                    liés à partir de son site.</p>

                <h2>Droit applicable et litiges</h2>
                <p>Les présentes mentions légales sont soumises au droit de France.
                    Tout litige sera de la compétence exclusive des tribunaux du commerce.</p>
            </div>
        </div>
    </section>


<?php include('../view/footer.php');