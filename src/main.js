import './asset/css/style.scss';
import './asset/css/main.scss';


document.addEventListener('DOMContentLoaded', function () {

    window.onload = function () {

        setTimeout(function () {
            var loadingScreen = document.querySelector('#loading-screen');
            loadingScreen.style.display = 'none';
        }, 1000);
    };
});

$(window).on('load', function () {
    $('#how').flexslider({
        animation: "slide",
        slideshowSpeed: 6000,
        animationSpeed: 3000,
        controlNav: false,
        directionNav: false,
        pauseOnHover: true
    });
});
$(window).on('load', function () {
    $('#actu').flexslider({
        animation: "carousel",
        slideshowSpeed: 6000,
        animationSpeed: 3000,
        controlNav: false,
        directionNav: false,
        pauseOnHover: true
    });
});
