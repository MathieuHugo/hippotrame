const inscription = document.querySelector('#inscription');

const inscription_modal = document.querySelector('#modal_inscription')
const connexion_modal = document.querySelector('#modal_connexion')

function closeInscription() {
    inscription_modal.style.display = "none";
}

function openConnexion() {
    connexion_modal.style.display = "flex";
}

inscription.addEventListener('submit', async (evt) => {
    evt.preventDefault();
    const data = new FormData(inscription);
    try {
        const res = await fetch('ajax/inscription.php', {
            method: 'POST',
            body: data
        });


        const response = await res.json();
        input_mail = document.querySelector('#input_mail');
        input_nom = document.querySelector('#input_nom');
        input_prenom = document.querySelector('#input_prenom');
        input_nom = document.querySelector('#input_nom');
        return_reg = document.querySelector('.return_reg');


        input_mail.innerText = `${response.errors['mail']}`;
        input_nom.innerText = `${response.errors['nom']}`;
        input_prenom.innerText = `${response.errors['prenom']}`;
        input_password.innerText = `${response.errors['password']}`;

        removeError('mail', input_mail);
        removeError('nom', input_nom);
        removeError('prenom', input_prenom);
        removeError('password', input_password);

        function removeError(type, element) {
            if (response.errors[type] == undefined) {
                element.classList.add("error_off");
            }
        }


        if (response.errors <= 0) {
            return_reg.style.display = "block";
            setTimeout(() => {
                closeInscription();
                openConnexion();
            }, 1000);
        }

    } catch (err) {
        console.log(err);

    }
});


