import './asset/css/style.scss';
import './asset/css/reset.scss';


$(window).on('load', function () {
    $('#diapoIndex').flexslider({
        animation: "slide",
        slideshowSpeed: 6000,
        animationSpeed: 3000,
    });
});


//MODAL CONNEXION

const inscription_button = document.querySelector('#button_inscription')
const inscription_modal = document.querySelector('#modal_inscription')
const close_modal_inscription = document.querySelector('#closeModalInscription')

function openInscription(e) {
    e.preventDefault();
    inscription_modal.style.display = "flex";
} 
function closeInscription(e) {
    e.preventDefault();
    inscription_modal.style.display = "none";
}

inscription_button.addEventListener("click", openInscription)
close_modal_inscription.addEventListener("click", closeInscription)

//MODAL CONNEXION

const connexion_button = document.querySelector('#button_connexion')
const connexion_modal = document.querySelector('#modal_connexion')
const close_modal_connexion = document.querySelector('#closeModalConnexion')

function openConnexion(e) {
    e.preventDefault();
    connexion_modal.style.display = "flex";
}
function closeConnexion(e) {
    e.preventDefault();
    connexion_modal.style.display = "none";
}

connexion_button.addEventListener("click", openConnexion)
close_modal_connexion.addEventListener("click", closeConnexion)


//MODAL CONTACT

const contact_button = document.querySelector('#button_contact');
const contact_modal = document.querySelector('#modal_contact');
const close_modal_contact = document.querySelector('#closeModalContact');

function openContact(e) {
    e.preventDefault();
    contact_modal.style.display = "flex";
}
function closeContact(e) {
    e.preventDefault();
    contact_modal.style.display = "none";
}

contact_button.addEventListener("click", openContact)
close_modal_contact.addEventListener("click", closeContact)

//INSCRIPTION TO CONNEXION

var inscToConnex = document.querySelector('#inscToConnex')
inscToConnex.addEventListener("click", function (e) {
    e.preventDefault();
    closeInscription(e);
    openConnexion(e);

});

//CONNEXION TO INSCRIPTION

var connexToInsc = document.querySelector('#connexToInsc')
connexToInsc.addEventListener("click", function (e) {
    e.preventDefault();
    closeConnexion(e);
    openInscription(e);
});

