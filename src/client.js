import './asset/css/client.scss';

// Use the data obtained from PHP

// fetch
async function getTrame() {
    const res = await fetch('/api/trames.json');
    const data = await res.json();
    //console.log(data)
    return data;
}

// FCT : Ne fonctionne que lorsque c'est écrit au-dessus de l'appelle de cette fonction, bizarre...

// function bigCount(data, keyExtractor) {
//
//     let counter = [];
//     labels.forEach(label => {
//
//         const filteredData = data.filter(dat => keyExtractor(dat) === label);
//         counter.push(filteredData.length);
//     });
//
//     return counter;
// }

function bigCount(data, keyExtractor) {

    let counter = [];
    labels.forEach(label => {

        const filteredData = data.filter(dat => keyExtractor(dat) === label);
        counter.push(filteredData.length);
    });

    return counter;
}

// Graphique du nombre de trames par type de requêtes

async function myBarChart() {
    const data = await getTrame();
    // console.log(data);
    var ctx = document.getElementById("myAreaChart");

    let labels = [...new Set(data
        .filter(dat => dat.protocol && dat.protocol.name)
        .map(dat => dat.protocol.name))];

    console.log(labels);

    let counter = [];
    labels.forEach(label => {

        console.log(label);

        const filtreer = data.filter(dat => dat.protocol.name === label);
        counter.push(filtreer.length)


    })


    var myLineChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                label: "Trames",
                backgroundColor: ['#4e73df', '#1cc88a', '#36b9cc', '#FF5F6D'],
                hoverBackgroundColor: ['#4162bf', '#1bb77e', '#32acbc', '#e8535f'],
                data: counter,
            }],
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                y: {
                    min: 0,
                    max: 1000,
                    ticks: {
                        stepSize: 250,
                    },
                },
                xAxes: [{

                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    offset: true,
                    ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                        callback: function(value, index, values) {
                            return number_format(value);
                        }
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: false
            },
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,
                callbacks: {
                    label: function(tooltipItem, chart) {
                        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                        return datasetLabel + ' : ' + number_format(tooltipItem.yLabel);
                    }
                }
            }
        }
    });
}


myBarChart();

// Status des requêtes

async function myPieChart() {
    const data = await getTrame();
    var ctx = document.getElementById("myPieChart");

    let status = [...new Set(data
        .filter(dat => dat.status)
        .map(dat => dat.status))];

    let count = [];
    status.forEach(statu => {

        console.log(statu);

        const filtreer = data.filter(dat => dat.status === statu);
        count.push(filtreer.length)


    })

    var myPieChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: status,
            datasets: [{
                data: count,
                backgroundColor: ['#e74a3b', '#f6c23e', '#36b9cc', '#1cc88a'],
                hoverBackgroundColor: ['#e74a3b', '#f6c23e', '#36b9cc', '#1cc88a'],
                hoverBorderColor: "rgba(234, 236, 244, 1)",
            }],
        },
        options: {
            maintainAspectRatio: false,
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                caretPadding: 10,
            },
            legend: {
                display: false
            },
            cutoutPercentage: 80,
        },
    });
}

myPieChart();

// Nombre total de TTL expirés
async function myTtl() {
    const data = await getTrame();

    var countTTLZero = 0;

    data.forEach(item => {
        if (item.ttl === 0) {
            countTTLZero++;
        }
    });

    let ttlResult = document.querySelector('.ttl-result');

    ttlResult.textContent = countTTLZero;
}

myTtl();

// Tableau des logs

$(document).ready(function() {
    console.log('Avant initialisation de DataTable');
    $('#dataTable').DataTable( {
        ajax: {
            url: '../api/trames.json',
            dataSrc: ''
        },
        columns: [
            { data: 'date', render: function(data) { return new Date(data * 1000).toLocaleString(); } },
            { data: 'version', render: function(data) {
                    return 'IPv' + data;
                } },
            { data: 'headerLength' },
            { data: 'identification' },
            { data: 'flags.code' },
            { data: 'ttl' },
            { data: 'protocol.name' },
            { data: 'protocol.flags.code' },
            { data: 'protocol.ports.from' },
            { data: 'protocol.ports.dest' },
            { data: 'ip.from' },
            { data: 'ip.dest' },
            { data: 'status' }
        ],
        columnDefs: [
            { targets: 0, title: 'Date' },
            { targets: 1, title: 'Version de l\'IP' },
            { targets: 2, title: 'Longueur en-tête' },
            { targets: 3, title: 'ID' },
            { targets: 4, title: '"Flags" : Code' },
            { targets: 5, title: 'TTL' },
            { targets: 6, title: 'Nom du protocole' },
            { targets: 7, title: '"Flags" : protocole' },
            { targets: 8, title: 'Port : source' },
            { targets: 9, title: 'Port : destination' },
            { targets: 10, title: 'IP : source' },
            { targets: 11, title: 'IP destination' },
            { targets: 12, title: 'Statut' }
        ]
    } );
});

// Activation de la version premium

let premiumLink = document.querySelector('.premium-link');
let premiumLink2 = document.querySelector('.premium-link-2');
let premiumBox = document.querySelector('.premium-box');
let premiumTable = document.querySelector('.premium-none');

premiumLink.addEventListener('click', function (evt) {
    evt.preventDefault();
    console.log("Clic sur le lien premium");

    if (premiumTable.style.display === 'none' || premiumTable.style.display === '') {
        premiumTable.style.display = 'block';
    } else {
        premiumTable.style.display = 'none';
    }

    premiumLink.style.display = 'none'
    premiumBox.style.display = 'none'

});

premiumLink2.addEventListener('click', function (evt) {
    evt.preventDefault();
    console.log("Clic sur le lien premium");

    if (premiumTable.style.display === 'none' || premiumTable.style.display === '') {
        premiumTable.style.display = 'block';
    } else {
        premiumTable.style.display = 'none';
    }

    premiumLink.style.display = 'none'
    premiumBox.style.display = 'none'

});