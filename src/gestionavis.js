import './asset/css/style.scss';
import './asset/css/main.scss';
import './asset/css/reset.scss';
import './asset/css/gestionavis.scss';


$(window).on('load', function() {
    dropdown()
    getAllAvis()
})
const sectionavis=document.querySelector('#gestionAvis')
const wrap=document.createElement('div')
wrap.classList.add('wrap')
const h1=document.createElement('h1')
h1.innerText="Liste et gestion des avis"
sectionavis.append(h1)
/////////////
//dropdown//
///////////
const boxDrop=document.createElement('div')
boxDrop.id="boxDrop"
sectionavis.append(boxDrop)
const etats=[
    {status: 'tout', titre:'Tout'},
    {status: 'publish',titre: 'Publié(s)'},
    {status: 'cache', titre: 'Caché(s)'}
]
function dropdown(){
    const btnDrop=document.createElement('a')
    btnDrop.href=""
    btnDrop.innerText=etats[0].titre
    boxDrop.dataset.id=etats[0].status
    btnDrop.classList.add("dropDown")
    const ul =document.createElement('ul')
    ul.classList.add("cache")
    boxDrop.appendChild(btnDrop)
    boxDrop.appendChild(ul)


    etats.forEach((e)=>{
        const li=document.createElement('li')
        const a=document.createElement('a')
        a.href=""
        a.innerText=e.titre;
        a.id=e.status
        li.appendChild(a)
        ul.appendChild(li)

        a.addEventListener("click",(evt)=>{
            evt.preventDefault()
            btnDrop.innerText=""
            btnDrop.innerText=a.innerText
            ul.classList.add('cache')
            boxDrop.dataset.id=e.status
            if(boxDrop.dataset.id == 'tout'){
                wrap.innerHTML=""
                getAllAvis()
            }else{
                wrap.innerHTML=""
                getAvisByStatus(boxDrop.dataset.id)
            }

        })
    })
    btnDrop.addEventListener('click',(evt)=>{
        evt.preventDefault()
        if(ul.classList.contains('cache')){
            ul.classList.remove('cache')
        }else{
            ul.classList.add('cache')
        }
    })
}


///////////////
//validation//
/////////////
function validationInput(id,min,max) {
    const input = document.querySelector(`#${id}`);
    const errorId = document.querySelector(`#error_${id}`);
    const submit=document.querySelector(`.submit`)
    input.addEventListener('keyup', (evt) => {
        let longueur = input.value.length
        if (longueur < min) {
            errorId.innerText = `Renseigner au minimum ${min} caractères`
            errorId.classList.add('nok')
            submit.disabled=true
        } else if (longueur > max) {
            errorId.innerText = `Renseigner au maximum ${max} caractères`
            errorId.classList.add('nok')
            submit.disabled=true
        } else {
            errorId.innerText =""
            submit.disabled=false
        }
    })
}


////////////////////////////
//affichage tous les avis//
//////////////////////////
async function getAllAvis(){
    try{
       const response=await fetch('../ajax/getAvis.php');
       const data = await response.json();

       data.forEach((d)=>{
       const avis=document.createElement('div')
       avis.dataset.id=d.id -1
       avis.classList.add('avis')

       const auteur=document.createElement('p')
       auteur.innerText=`${d.prenom} ${d.nom} ( ${d.job} )`


       const dateAt=document.createElement('p')
       if(d.modified_at==null){
           dateAt.innerText=`Ecrit le: ${d.created_at}`
       }else{
           dateAt.innerText=`Ecrit le: ${d.created_at}, modifié le ${d.modified_at}`
       }

       const titre=document.createElement('p')
       titre.innerText=d.title

       const message=document.createElement('p')
       message.innerText=d.message

       const etat=document.createElement('p')
       etat.innerText=`Etat: ${d.etat}`

        avis.appendChild(auteur)
        avis.appendChild(dateAt)
        avis.appendChild(titre)
        avis.appendChild(message)
        avis.appendChild(etat)
        wrap.appendChild(avis)

///////////////////////////
//event vers single avis//
/////////////////////////
           avis.addEventListener('click',(evt)=>{
               wrap.innerHTML=""
               getAvisByClick(avis.dataset.id)
               $('html,body').animate({scrollTop: 0});
           })
//fin event//
       })
        sectionavis.append(wrap)
    }catch (err) {
    console.log(err)
    }
}


////////////////
//Single avis//
//////////////
async function getAvisByClick(id){
    try {
        const response = await fetch('../ajax/getAvis.php');
        const data = await response.json();
        const singleavis = data[id]

        const form=document.createElement('form')
        form.id="formModifAvis"
        form.method="POST"
        form.noValidate=true

        const labelnom=document.createElement('label')
        labelnom.htmlFor="nom"
        labelnom.innerText="Nom de l'auteur:"
        const nom =document.createElement('input')
        nom.type="text"
        nom.value= singleavis.nom
        nom.name="nom"
        nom.id="nom"
        const errornom=document.createElement('span')
        errornom.classList.add('error')
        errornom.id="error_nom"

        const labelprenom=document.createElement('label')
        labelprenom.htmlFor="prenom"
        labelprenom.innerText="Prenom de l'auteur:"
        const prenom =document.createElement('input')
        nom.type="text"
        prenom.value= singleavis.prenom
        prenom.name="prenom"
        prenom.id="prenom"
        const errorprenom=document.createElement('span')
        errorprenom.classList.add('error')
        errorprenom.id="error_prenom"

        const labeljob=document.createElement('label')
        labeljob.htmlFor="job"
        labeljob.innerText="Fonction de l'auteur:"
        const job =document.createElement('input')
        job.type="text"
        job.value= singleavis.job
        job.name="job"
        job.id="job"
        const errorjob=document.createElement('span')
        errorjob.classList.add('error')
        errorjob.id="error_job"

        const labeltitre=document.createElement('label')
        labeltitre.htmlFor="titre"
        labeltitre.innerText="Titre de l'avis:"
        const titre =document.createElement('input')
        titre.type="text"
        titre.value= singleavis.title
        console.log(titre.value)
        titre.name="titre"
        titre.id="titre"
        const errortitre=document.createElement('span')
        errortitre.classList.add('error')
        errortitre.id="error_titre"

        const labelmessage=document.createElement('label')
        labelmessage.htmlFor="message"
        labelmessage.innerText="Contenu de l'avis:"
        const message =document.createElement('textarea')
        message.innerText= singleavis.message
        message.name="message"
        message.id="message"
        const errormessage=document.createElement('span')
        errormessage.classList.add('error')
        errormessage.id="error_message"

        const labeletat=document.createElement('label')
        labelnom.htmlFor="etat"
        labeletat.innerText="Etat de l'avis:"
        const etat=document.createElement('select')
        etat.name='etat'
        const publish=document.createElement('option')
        publish.value='publish'
        publish.innerText="publish"
        const cache= document.createElement('option')
        cache.value='cache'
        cache.innerText='cache'
        if (singleavis.etat === 'publish') {
            publish.selected = true;
        } else if (singleavis.etat === 'cache') {
            cache.selected = true;
        }

        const inputid=document.createElement('input')
        inputid.name="id"
        inputid.id="id"
        inputid.type="hidden"
        inputid.value=singleavis.id

        const submit=document.createElement('input')
        submit.type='submit'
        submit.id='submit'
        submit.classList.add('submit')
        submit.name='modifier'

        form.appendChild(labelnom)
        form.appendChild(nom)
        form.appendChild(errornom)
        form.appendChild(labelprenom)
        form.appendChild(prenom)
        form.appendChild(errorprenom)
        form.appendChild(labeljob)
        form.appendChild(job)
        form.appendChild(errorjob)
        form.appendChild(labeltitre)
        form.appendChild(titre)
        form.appendChild(errortitre)
        form.appendChild(labelmessage)
        form.appendChild(message)
        form.appendChild(errormessage)
        form.appendChild(labeletat)
        form.appendChild(etat)
        etat.appendChild(publish)
        etat.appendChild(cache)
        form.appendChild(inputid)
        form.appendChild(submit)
        wrap.append(form)

        validationInput('nom',2,30)
        validationInput('prenom',2,30)
        validationInput('job',2,30)
        validationInput('titre',2,30)
        validationInput('message',2,120)

////////////////////////////
//requête ajax modif avis//
//////////////////////////
        const modifavis=document.querySelector('#formModifAvis')
        modifavis.addEventListener('submit',async (evt) => {
            evt.preventDefault()
            const formdata =new FormData(modifavis);
            try {
                const res = await fetch('../ajax/updateAvis.php',{
                    method: 'POST',
                    body: formdata
                });
                const json = await res.json();

                if (json.errors <= 0) {
                    wrap.innerHTML=""
                    getAllAvis()
                    $('html,body').animate({scrollTop: 0});
                }
            }catch(err){
                console.log(err)
            }
        });
    }catch (err) {
        console.log(err)
    }
}
/////////////////////////
// event creation avis//
///////////////////////
const btnCreatAvis=document.createElement('div')
btnCreatAvis.classList.add("btn")
btnCreatAvis.classList.add("crea")
btnCreatAvis.innerText="Ajouter un avis"
sectionavis.append(btnCreatAvis)
btnCreatAvis.addEventListener("click",(evt)=>{
    if(btnCreatAvis.classList.contains('crea')){
        btnCreatAvis.innerText="Retour aux avis"
        btnCreatAvis.classList.remove('crea')
        h1.innerText="Ajouter un avis"
        wrap.innerHTML=""
        creatAvis()
    }else{
        btnCreatAvis.classList.add("crea")
        btnCreatAvis.innerText="Ajouter un avis"
        wrap.innerHTML=""
        h1.innerText="Liste et gestion des avis"
        getAllAvis()
    }
})
//fin event//
//////////////////
//creation avis//
////////////////
function creatAvis(){
        const form=document.createElement('form')
        form.id="formCreatAvis"
        form.method="POST"
        form.noValidate=true

        const labelnom=document.createElement('label')
        labelnom.htmlFor="nom"
        labelnom.innerText="Nom de l'auteur:"
        const nom =document.createElement('input')
        nom.type="text"
        nom.name="nom"
        nom.id="nom"
        const errornom=document.createElement('span')
        errornom.classList.add('error')
        errornom.id="error_nom"

        const labelprenom=document.createElement('label')
        labelprenom.htmlFor="prenom"
        labelprenom.innerText="Prenom de l'auteur:"
        const prenom =document.createElement('input')
        nom.type="text"
        prenom.name="prenom"
        prenom.id="prenom"
        const errorprenom=document.createElement('span')
        errorprenom.classList.add('error')
        errorprenom.id="error_prenom"
        const labeljob=document.createElement('label')
        labeljob.htmlFor="job"
        labeljob.innerText="Fonction de l'auteur:"
        const job =document.createElement('input')
        nom.type="text"
        job.name="job"
        job.id="job"
        const errorjob=document.createElement('span')
        errorjob.classList.add('error')
        errorjob.id="error_job"

        const labeltitre=document.createElement('label')
        labeltitre.htmlFor="titre"
        labeltitre.innerText="Titre de l'avis:"
        const titre =document.createElement('input')
        nom.type="text"
        titre.name="titre"
        titre.id="titre"
        const errortitre=document.createElement('span')
        errortitre.classList.add('error')
        errortitre.id="error_titre"

        const labelmessage=document.createElement('label')
        labelmessage.htmlFor="message"
        labelmessage.innerText="Contenu de l'avis:"
        const message =document.createElement('textarea')
        message.name="message"
        message.id="message"
        const errormessage=document.createElement('span')
        errormessage.classList.add('error')
        errormessage.id="error_message"

        const etat=document.createElement('input')
        etat.name='etat'
        etat.value="cache"
        etat.type="hidden"
    console.log(etat)

        const submit=document.createElement('input')
        submit.type='submit'
        submit.id='submit'
        submit.classList.add('submit')
        submit.name='submitted'


    form.appendChild(labelnom)
    form.appendChild(nom)
    form.appendChild(errornom)
    form.appendChild(labelprenom)
    form.appendChild(prenom)
    form.appendChild(errorprenom)
    form.appendChild(labeljob)
    form.appendChild(job)
    form.appendChild(errorjob)
    form.appendChild(labeltitre)
    form.appendChild(titre)
    form.appendChild(errortitre)
    form.appendChild(labelmessage)
    form.appendChild(message)
    form.appendChild(errormessage)
    form.appendChild(etat)
    form.appendChild(submit)
    wrap.append(form)

    validationInput('nom',2,30)
    validationInput('prenom',2,30)
    validationInput('job',2,30)
    validationInput('titre',2,30)
    validationInput('message',2,120)

////////////////////////////
//requête ajax ajout avis//
//////////////////////////
    const ajoutavis=document.querySelector('#formCreatAvis')
    ajoutavis.addEventListener('submit',async (evt) => {
        evt.preventDefault()
        const formdata =new FormData(ajoutavis);
        try {
            const res = await fetch('../ajax/insertAvis.php',{
                method: 'POST',
                body: formdata
            });
            const json = await res.json();
            if (json.errors <= 0) {
                btnCreatAvis.classList.add("crea")
                btnCreatAvis.innerText="Ajouter un avis"
                wrap.innerHTML=""
                h1.innerText="Liste et gestion des avis"
                getAllAvis()
                $('html,body').animate({scrollTop: 0});
            }
        }catch(err){
            console.log(err)
        }
    });
}
//////////////////////////////
//affichage avis par status//
////////////////////////////
async function getAvisByStatus(status){
    try{
        const response=await fetch('../ajax/getAvis.php');
        const data = await response.json();

        data.forEach((d)=>{
            if (d.etat == status) {
                const avis = document.createElement('div')
                avis.dataset.id = d.id - 1
                avis.classList.add('avis')

                const auteur = document.createElement('p')
                auteur.innerText = `${d.prenom} ${d.nom} ( ${d.job} )`


                const dateAt = document.createElement('p')
                if (d.modified_at == null) {
                    dateAt.innerText = `Ecrit le: ${d.created_at}`
                } else {
                    dateAt.innerText = `Ecrit le: ${d.created_at}, modifié le ${d.modified_at}`
                }

                const titre = document.createElement('p')
                titre.innerText = d.title

                const message = document.createElement('p')
                message.innerText = d.message

                const etat = document.createElement('p')
                etat.innerText = `Etat: ${d.etat}`

                avis.appendChild(auteur)
                avis.appendChild(dateAt)
                avis.appendChild(titre)
                avis.appendChild(message)
                avis.appendChild(etat)
                wrap.appendChild(avis)

///////////////////////////
//event vers single avis//
/////////////////////////
                avis.addEventListener('click', (evt) => {
                    wrap.innerHTML = ""
                    getAvisByClick(avis.dataset.id)
                    $('html,body').animate({scrollTop: 0});
                })
//fin event//
            }
        })
        sectionavis.append(wrap)
    }catch (err) {
        console.log(err)
    }
}

