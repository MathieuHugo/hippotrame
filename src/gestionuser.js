import './asset/css/style.scss';
import './asset/css/main.scss';
import './asset/css/reset.scss';
import './asset/css/gestionuser.scss'

$(window).on('load', function() {
    dropdown()
    getAllUsers()
})
const sectionuser=document.querySelector('#gestionUsers')
const wrap=document.createElement('div')
wrap.classList.add('wrap')
const h1=document.createElement('h1')
h1.innerText="Liste et gestion des utilisateurs"
sectionuser.append(h1)
/////////////
//dropdown//
///////////
const boxDrop=document.createElement('div')
boxDrop.id="boxDrop"
sectionuser.append(boxDrop)
const etats=[
    {status: 'tout', titre: 'Tout'},
    {status: 'admin', titre:'Admin'},
    {status: 'user', titre: 'Utilisateur'}
]
function dropdown(){
    const btnDrop=document.createElement('a')
    btnDrop.href=""
    btnDrop.innerText=etats[0].titre
    boxDrop.dataset.id=etats[0].status
    btnDrop.classList.add("dropDown")
    const ul =document.createElement('ul')
    ul.classList.add("cache")
    boxDrop.appendChild(btnDrop)
    boxDrop.appendChild(ul)


    etats.forEach((e)=>{
        const li=document.createElement('li')
        const a=document.createElement('a')
        a.href=""
        a.innerText=e.titre;
        a.id=e.status
        li.appendChild(a)
        ul.appendChild(li)

        a.addEventListener("click",(evt)=>{
            evt.preventDefault()
            btnDrop.innerText=""
            btnDrop.innerText=a.innerText
            ul.classList.add('cache')
            boxDrop.dataset.id=e.status
            if(boxDrop.dataset.id == 'tout'){
                wrap.innerHTML=""
                getAllUsers()
            }else{
                wrap.innerHTML=""
                getUsersByStatus(boxDrop.dataset.id)
            }

        })
    })
    btnDrop.addEventListener('click',(evt)=>{
        evt.preventDefault()
        if(ul.classList.contains('cache')){
            ul.classList.remove('cache')
        }else{
            ul.classList.add('cache')
        }
    })
}
///////////////
//validation//
/////////////
function validationInput(id,min,max) {
    const input = document.querySelector(`#${id}`);
    const errorId = document.querySelector(`#error_${id}`);
    const submit=document.querySelector(`.submit`)
    input.addEventListener('keyup', (evt) => {
        let longueur = input.value.length
        if (longueur < min) {
            errorId.innerText = `Renseigner au minimum ${min} caractères`
            errorId.classList.add('nok')
            submit.disabled=true
        } else if (longueur > max) {
            errorId.innerText = `Renseigner au maximum ${max} caractères`
            errorId.classList.add('nok')
            submit.disabled=true
        } else {
            errorId.innerText =""
            submit.disabled=false
        }
    })
}
/////////////////////////////
//affichage tous les users//
///////////////////////////
async function getAllUsers(){
    try{
        const response=await fetch('../ajax/getUsers.php');
        const data=await response.json();
        data.forEach((d)=> {

            const boxUser = document.createElement('div')
            boxUser.classList.add('boxUser')
            boxUser.dataset.id = d.id - 1

            const nom = document.createElement("p")
            nom.innerText =`Nom: ${d.nom}`

            const prenom = document.createElement("p")
            prenom.innerText =`Prenom: ${d.prenom}`

            const mail = document.createElement("p")
            mail.innerText=`adresse mail: ${d.mail}`

            const dateAt=document.createElement("p")
            if(d.modified_at==null){
                dateAt.innerText=`Ecrit le: ${d.created_at}`
            }else{
                dateAt.innerText=`Ecrit le: ${d.created_at}, modifié le ${d.modified_at}`
            }

            const status =document.createElement("p")
            status.innerText=`statut: ${d.status}`

            boxUser.appendChild(nom)
            boxUser.appendChild(prenom)
            boxUser.appendChild(mail)
            boxUser.appendChild(dateAt)
            boxUser.appendChild(status)
            wrap.appendChild(boxUser)

///////////////////////////
//event vers single user//
/////////////////////////
             boxUser.addEventListener('click',(evt)=>{
                 wrap.innerHTML=""
                 getUserByClick(boxUser.dataset.id)
                 $('html,body').animate({scrollTop: 0});
             })
//fin event//
        })
        sectionuser.append(wrap)
    }
    catch (err) {
        console.log(err)
    }
}
////////////////
//Single user//
//////////////
async function getUserByClick(id){
    try {
        const response = await fetch('../ajax/getUsers.php');
        const data = await response.json();
        const user = data[id]

        const form = document.createElement('form')
        form.id = "formModifUser"
        form.method = "POST"
        form.noValidate = true

        const labelnom = document.createElement('label')
        labelnom.htmlFor = "nom"
        labelnom.innerText = "Nom:"
        const nom = document.createElement('input')
        nom.type = "text"
        nom.value = user.nom
        nom.name = "nom"
        nom.id = "nom"
        const errornom = document.createElement('span')
        errornom.classList.add('error')
        errornom.id = "error_nom"

        const labelprenom = document.createElement('label')
        labelprenom.htmlFor = "prenom"
        labelprenom.innerText = "Prenom:"
        const prenom = document.createElement('input')
        nom.type = "text"
        prenom.value = user.prenom
        prenom.name = "prenom"
        prenom.id = "prenom"
        const errorprenom = document.createElement('span')
        errorprenom.classList.add('error')
        errorprenom.id = "error_prenom"

        const labelmail = document.createElement('label')
        labelmail.htmlFor = "mail"
        labelmail.innerText = "Adresse mail:"
        const mail = document.createElement('input')
        mail.type = "text"
        mail.value = user.mail
        mail.name = "mail"
        mail.id = "mail"


        const labelstatus = document.createElement('label')
        labelnom.htmlFor = "status"
        labelstatus.innerText = "status:"
        const status = document.createElement('select')
        status.name = 'status'
        const utilisateur = document.createElement('option')
        utilisateur.value = 'user'
        utilisateur.innerText = "utilisateur"
        const admin = document.createElement('option')
        admin.value = 'admin'
        admin.innerText = 'admin'
        if (user.status === 'user') {
            utilisateur.selected = true;
        } else if (user.status === 'admin') {
            admin.selected = true;
        }
            const inputid = document.createElement('input')
            inputid.name = "id"
            inputid.id = "id"
            inputid.type = "hidden"
            inputid.value = user.id

            const submit = document.createElement('input')
            submit.type = 'submit'
            submit.id = 'submit'
            submit.classList.add('submit')
            submit.name = 'modifier'

            form.appendChild(labelnom)
            form.appendChild(nom)
            form.appendChild(errornom)
            form.appendChild(labelprenom)
            form.appendChild(prenom)
            form.appendChild(errorprenom)
            form.appendChild(labelmail)
            form.appendChild(mail)
            form.appendChild(labelstatus)
            form.appendChild(status)
            status.appendChild(utilisateur)
            status.appendChild(admin)
            form.appendChild(inputid)
            form.appendChild(submit)
            wrap.append(form)

            validationInput('nom', 2, 50)
            validationInput('prenom', 2, 50)



////////////////////////////
//requête ajax modif avis//
//////////////////////////
            const modifuser = document.querySelector('#formModifUser')
            modifuser.addEventListener('submit', async (evt) => {
                evt.preventDefault()
                const formdata = new FormData(modifuser);
                try {
                    const res = await fetch('../ajax/updateUser.php', {
                        method: 'POST',
                        body: formdata
                    });
                    const json = await res.json();

                    if (json.errors <= 0) {
                        wrap.innerHTML = ""
                        getAllUsers()
                        $('html,body').animate({scrollTop: 0});
                    }
                } catch (err) {
                    console.log(err)
                }
            });
    }catch(err){
        console.log(err)
    }
}
////////////////////////////////
//affichage  users par status//
//////////////////////////////
async function getUsersByStatus(status){
    try{
        const response=await fetch('../ajax/getUsers.php');
        const data=await response.json();
        data.forEach((d)=> {
            if (d.status == status) {
                const boxUser = document.createElement('div')
                boxUser.classList.add('boxUser')
                boxUser.dataset.id = d.id - 1

                const nom = document.createElement("p")
                nom.innerText = `Nom: ${d.nom}`

                const prenom = document.createElement("p")
                prenom.innerText = `Prenom: ${d.prenom}`

                const mail = document.createElement("p")
                mail.innerText = `adresse mail: ${d.mail}`

                const dateAt = document.createElement("p")
                if (d.modified_at == null) {
                    dateAt.innerText = `Ecrit le: ${d.created_at}`
                } else {
                    dateAt.innerText = `Ecrit le: ${d.created_at}, modifié le ${d.modified_at}`
                }

                const status = document.createElement("p")
                status.innerText = `statut: ${d.status}`

                boxUser.appendChild(nom)
                boxUser.appendChild(prenom)
                boxUser.appendChild(mail)
                boxUser.appendChild(dateAt)
                boxUser.appendChild(status)
                wrap.appendChild(boxUser)

///////////////////////////
//event vers single user//
/////////////////////////
                boxUser.addEventListener('click', (evt) => {
                    wrap.innerHTML = ""
                    getUserByClick(boxUser.dataset.id)
                    $('html,body').animate({scrollTop: 0});
                })
//fin event//
            }
        })
        sectionuser.append(wrap)
    }
    catch (err) {
        console.log(err)
    }
}