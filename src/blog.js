import './asset/css/style.scss';
import './asset/css/main.scss';
import './asset/css/reset.scss';
import './asset/css/blog.scss';

$(window).on('load', function() {
    getArticlesPublish()
    getAvisPublish()
})

/////////////////
//--ARTICLES--//
///////////////
const articlesBlog=document.querySelector('#articlesblog')

const h1 = document.createElement('h1')
h1.classList.add('titresection')
h1.innerText = "Qu'est ce qu'il se trame?"
articlesBlog.append(h1)
async function getArticlesPublish(){
    try{
        const response=await fetch('ajax/getArticles.php');
        const data=await response.json();

        const wrap = document.createElement('div')
        wrap.classList.add('wrap2')

            data.forEach((d) => {
                if (d.status == 'publish') {
                    const boxArticle = document.createElement('div')
                    boxArticle.classList.add('boxArticle')
                    boxArticle.dataset.id = d.id - 1
                    console.log(boxArticle.dataset)
                    const img = document.createElement("img")
                    const lien = `asset/media/${d.image}`
                    img.src = lien
                    const titre = d.titre
                    img.alt = `${titre}`


                    const boxExtraitArticle = document.createElement("div")
                    boxExtraitArticle.classList.add("boxExtraitArticle")

                    const h3 = document.createElement("h3")
                    h3.innerText = titre

                    const h4 = document.createElement("h4")
                    h4.innerText = `${d.extrait}[...]`

                    const auteur = document.createElement("p")
                    auteur.classList.add('auteur')
                    auteur.innerText = d.auteur

                    boxArticle.appendChild(img)
                    boxArticle.appendChild(boxExtraitArticle)
                    boxExtraitArticle.appendChild(h3)
                    boxExtraitArticle.appendChild(h4)
                    boxExtraitArticle.appendChild(auteur)
                    wrap.appendChild(boxArticle)

                    boxArticle.addEventListener('click', (evt) => {
                        articlesBlog.innerHTML = ""
                        getArticleByClick(boxArticle.dataset.id)
                        $('html,body').animate({scrollTop: 0});
                    })
                }
            })
                articlesBlog.append(wrap)


    }catch (err) {
        console.log(err)
    }
}


async function getArticleByClick(id){
    try{
    const response=await fetch('ajax/getArticles.php');
    const data=await response.json();
    const art=data[id]

        const wrap = document.createElement('div')
        wrap.classList.add('wrap2')

        const boxArticle = document.createElement('div')
        boxArticle.classList.add('boxArticle')

    const img=document.createElement("img")
    const lien= `asset/media/${art.image}`
    img.src=lien
    const titre= art.titre
    img.alt=titre


    const boxExtraitArticle = document.createElement("div")
    boxExtraitArticle.classList.add("boxExtraitArticle")

    const h3 = document.createElement("h3")
    h3.innerText = titre

        const h4 = document.createElement("h4")
        h4.innerText = `${art.extrait}[...]`

        const boxTexteArticle = document.createElement('div')
        boxTexteArticle.classList.add("boxTexteArticle")

        const contenu = document.createElement("p")
        contenu.classList.add('content')
        contenu.innerText = art.content

    const auteur=document.createElement("p")
    auteur.classList.add('auteur')
    auteur.innerText=art.auteur

    const btnRetour=document.createElement("div")
    btnRetour.classList.add("btnretour")
    btnRetour.innerText="Retour aux actualités"
        btnRetour.addEventListener("click",(evt)=>{
        wrap.innerHTML=""
        getArticlesPublish()
        $('html,body').animate({scrollTop: 0});
        })

        boxArticle.appendChild(img)
        boxArticle.appendChild(boxExtraitArticle)
        boxExtraitArticle.appendChild(h3)
        boxExtraitArticle.appendChild(h4)
        boxExtraitArticle.appendChild(auteur)
        boxTexteArticle.appendChild(contenu)

        wrap.appendChild(boxArticle)
        wrap.appendChild(boxTexteArticle)
        wrap.append(btnRetour)
        articlesBlog.append(wrap)

    }
    catch (err) {
        console.log(err)
    }
}

////////////////////
//flexslider avis//
//////////////////
const avisBlog=document.querySelector('#avisblog')

async function getAvisPublish(){
    try {
        const response = await fetch('ajax/getAvis.php');
        const data = await response.json();
        const wrap=document.createElement('div')
        wrap.classList.add('wrap2')
        const slides=document.createElement('ul')
        slides.classList.add('slides')
        const flexslider=document.createElement('div')
        flexslider.classList.add('flexslider')
        flexslider.id='diapoavis'
        const h2 = document.createElement('h2')
        h2.classList.add('titresection')
        h2.innerText = "Ils nous font confiance"

        data.forEach((d)=>{
            if(d.etat == 'publish'){
                console.log('ok')
                    const li = document.createElement('li')
                    const div = document.createElement('div')

                        const titre = document.createElement('p')
                        titre.classList.add('titreavis')
                        titre.innerText = d.title

                        const avis = document.createElement('p')
                        avis.classList.add('avis')
                        avis.innerText = d.message

                        const auteur = document.createElement('p')
                        auteur.classList.add('auteuravis')
                        auteur.innerText = `${d.prenom} ${d.nom}( ${d.job} ) `


                        div.appendChild(titre)
                        div.appendChild(avis)
                        div.appendChild(auteur)
                        li.appendChild(div)
                        slides.appendChild(li)
            }
        })

        flexslider.appendChild(slides)
        wrap.appendChild(flexslider)
        avisBlog.append(h2)
        avisBlog.append(wrap)
    }catch (err) {
        console.log(err)
    }

    $('#diapoavis').flexslider({
        animation: "slide",
        slideshowSpeed: 6000,
        animationSpeed: 3000,
        directionNav: false,
    });
}
