const connexion = document.querySelector('#connexion');

connexion.addEventListener('submit', async (evt) => {
    evt.preventDefault();
    const data = new FormData(connexion);
    try {
        const res = await fetch('ajax/connexion.php', {
            method: 'POST',
            body: data
        });

        const response = await res.json();

        connex_input_mail = document.querySelector('#connex_input_mail');
        connex_input_password = document.querySelector('#connex_input_password');

        removeError('mail', connex_input_mail);
        removeError('password', connex_input_password);

        function removeError(type, element) {
            if (response.errors[type] == undefined) {
                element.classList.add("error_off");
            }
        }

        connex_input_mail.innerText = `${response.errors['mail']}`;
        connex_input_password.innerText = `${response.errors['login']}`;

        console.log(response);

        if (response.success == true) {
            location.reload();
        }



    } catch (err) {
        console.log(err);
    }
});
