import './asset/css/style.scss';
import './asset/css/main.scss';
import './asset/css/reset.scss';
import './asset/css/blog.scss'
import './asset/css/gestionblog.scss'

$(window).on('load', function() {
    dropdown()
    getAllArticles()
})
const articlesBlog=document.querySelector('#gestionBlog')
const wrap = document.createElement('div')
wrap.classList.add('wrap')
const h1=document.createElement('h1')
h1.innerText="Liste et gestion des articles"
articlesBlog.append(h1)
/////////////
//dropdown//
///////////
const boxDrop=document.createElement('div')
boxDrop.id="boxDrop"
articlesBlog.append(boxDrop)
const etats=[
    {status: 'tout', titre:'Tout'},
    {status: 'publish',titre: 'Publié(s)'},
    {status: 'cache', titre: 'Caché(s)'}
]
function dropdown(){
    const btnDrop=document.createElement('a')
    btnDrop.href=""
    btnDrop.innerText=etats[0].titre
    boxDrop.dataset.id=etats[0].status
    btnDrop.classList.add("dropDown")
    const ul =document.createElement('ul')
    ul.classList.add("cache")
    boxDrop.appendChild(btnDrop)
    boxDrop.appendChild(ul)


    etats.forEach((e)=>{
        const li=document.createElement('li')
        const a=document.createElement('a')
        a.href=""
        a.innerText=e.titre;
        a.id=e.status
        li.appendChild(a)
        ul.appendChild(li)

        a.addEventListener("click",(evt)=>{
            evt.preventDefault()
            btnDrop.innerText=""
            btnDrop.innerText=a.innerText
            ul.classList.add('cache')
            boxDrop.dataset.id=e.status
            if(boxDrop.dataset.id == 'tout'){
                wrap.innerHTML=""
                getAllArticles()
            }else{
                wrap.innerHTML=""
                getArticlesByStatus(boxDrop.dataset.id)
            }
        })
    })
    btnDrop.addEventListener('click',(evt)=>{
        evt.preventDefault()
        if(ul.classList.contains('cache')){
            ul.classList.remove('cache')
        }else{
            ul.classList.add('cache')
        }
    })
}



///////////////
//validation//
/////////////
function validationInput(id,min,max) {
    const input = document.querySelector(`#${id}`);
    const errorId = document.querySelector(`#error_${id}`);
    const submit=document.querySelector(`.submit`)
    input.addEventListener('keyup', (evt) => {
        let longueur = input.value.length
        if (longueur < min) {
            errorId.innerText = `Renseigner au minimum ${min} caractères`
            errorId.classList.add('nok')
            submit.disabled=true
        } else if (longueur > max) {
            errorId.innerText = `Renseigner au maximum ${max} caractères`
            errorId.classList.add('nok')
            submit.disabled=true
        } else {
            errorId.innerText =""
            submit.disabled=false
        }
    })
}
////////////////////////////////
//affichage tous les articles//
//////////////////////////////
async function getAllArticles(){
    try{
        const response=await fetch('../ajax/getArticles.php');
        const data=await response.json();

            data.forEach((d)=> {
                const boxArticle = document.createElement('div')
                boxArticle.classList.add('boxArticle')
                boxArticle.dataset.id = d.id - 1

                const img = document.createElement("img")
                const lien = `../asset/media/${d.image}`
                img.src = lien
                const titre = d.titre
                img.alt = `${titre}`

                const boxExtraitArticle = document.createElement("div")
                boxExtraitArticle.classList.add("boxExtraitArticle")

                const h3 = document.createElement("h3")
                h3.innerText = titre

                const h4 = document.createElement("h4")
                h4.innerText = `${d.extrait}[...]`

                const auteur = document.createElement("p")
                auteur.classList.add('auteur')
                auteur.innerText = d.auteur

                  boxArticle.appendChild(img)
                  boxArticle.appendChild(boxExtraitArticle)
                  boxExtraitArticle.appendChild(h3)
                  boxExtraitArticle.appendChild(h4)
                  boxExtraitArticle.appendChild(auteur)
                  wrap.appendChild(boxArticle)

//////////////////////////////
//event vers single article//
////////////////////////////
                boxArticle.addEventListener('click',(evt)=>{
                    wrap.innerHTML=""
                    getArticleByClick(boxArticle.dataset.id)
                    $('html,body').animate({scrollTop: 0});
                })
//fin event//
            })
            articlesBlog.append(wrap)
    }
    catch (err) {
        console.log(err)
    }
}

///////////////////
//single article//
/////////////////
async function getArticleByClick(id){
    try{
        const response= await fetch('../ajax/getArticles.php');
        const data=await response.json();
        const art=data[id]

        const form=document.createElement('form')
        form.id="formModifArticle"
        form.method="POST"
        form.noValidate=true

        const boximage = document.createElement('div')
        boximage.classList.add('boximage')

        const img=document.createElement("img")
        const lien= `../asset/media/${art.image}`
        img.src=lien
        const alt= art.titre
        img.alt=alt

        const boxChangeImg=document.createElement('div')
        boxChangeImg.classList.add("changeimg")
        const labelimage=document.createElement('label')
        labelimage.htmlFor="image"
        labelimage.classList.add("imgactuel")
        labelimage.innerText=`image actuelle: ${art.image}`
        const image =document.createElement('input')
        image.type="hidden"
        image.value= art.image
        image.name="image"
        image.id="image"
        const errorimage=document.createElement('span')
        errorimage.classList.add('error')
        errorimage.id="error_image"

        const labelupload=document.createElement('label')
        labelupload.htmlFor="upload"
        labelupload.innerText="uploader une nouvelle image (png,jpeg ou jpg)"
        const upload=document.createElement('input')
        upload.type="file"
        upload.name="upload"
        upload.id="upload"
        upload.accept=".png, .jpg, .jpeg"
/////////////////////
//sélection upload//
///////////////////
        upload.addEventListener("change",(evt)=>{
            if (upload.files.length ==1) {
                image.value = upload.files[0].name
            } else {
                image.value = art.image
            }
            upload.addEventListener("click",(evt)=>{
                upload.value=""
            })
        })
//fin event//
        const errorupload=document.createElement('span')
        errorupload.classList.add('error')
        errorupload.id="error_upload"
        console.log(upload.files.length)

        const labeltitre=document.createElement('label')
        labeltitre.htmlFor="titre"
        labeltitre.innerText="Titre de l'article"
        const titre =document.createElement('input')
        titre.type="text"
        titre.value= art.titre
        titre.name="titre"
        titre.id="titre"
        const errortitre=document.createElement('span')
        errortitre.classList.add('error')
        errortitre.id="error_titre"

        const labelextrait=document.createElement('label')
        labelextrait.htmlFor="extrait"
        labelextrait.innerText="Extrait de l'avis"
        const extrait =document.createElement('input')
        extrait.type="text"
        extrait.value= art.extrait
        extrait.name="extrait"
        extrait.id="extrait"
        const errorextrait=document.createElement('span')
        errorextrait.classList.add('error')
        errorextrait.id="error_extrait"

        const labelauteur=document.createElement('label')
        labelauteur.htmlFor="auteur"
        labelauteur.innerText="Auteur de l'article"
        const auteur =document.createElement('input')
        auteur.type="text"
        auteur.value= art.auteur
        auteur.name="auteur"
        auteur.id="auteur"
        const errorauteur=document.createElement('span')
        errorauteur.classList.add('error')
        errorauteur.id="error_auteur"

        const labelcontenu=document.createElement('label')
        labelcontenu.htmlFor="contenu"
        labelcontenu.innerText="Contenu de l'article"
        const contenu =document.createElement('textarea')
        contenu.innerText= art.content
        contenu.name="contenu"
        contenu.id="contenu"
        const errorcontenu=document.createElement('span')
        errorcontenu.classList.add('error')
        errorcontenu.id="error_contenu"

        const labelstatus=document.createElement('label')
        labelstatus.htmlFor="status"
        labelstatus.innerText="status de l'article"
        const status=document.createElement('select')
        status.name='status'
        const publish=document.createElement('option')
        publish.value='publish'
        publish.innerText="publish"
        const cache= document.createElement('option')
        cache.value='cache'
        cache.innerText='cache'
        if (art.status === 'publish') {
            publish.selected = true;
        } else if (art.status === 'cache') {
            cache.selected = true;
        }

        const inputid=document.createElement('input')
        inputid.name="id"
        inputid.id="id"
        inputid.type="hidden"
        inputid.value=art.id

        const submit=document.createElement('input')
        submit.type='submit'
        submit.id='submit'
        submit.classList.add('submit')
        submit.name='modifier'

        boximage.appendChild(img)
        boxChangeImg.appendChild(labelimage)
        boxChangeImg.appendChild(image)
        boxChangeImg.appendChild(errorimage)
        boxChangeImg.appendChild(labelupload)
        boxChangeImg.appendChild(upload)
        boxChangeImg.appendChild(errorupload)
        boximage.appendChild(boxChangeImg)
        form.appendChild(boximage)
        form.appendChild(labeltitre)
        form.appendChild(titre)
        form.appendChild(errortitre)
        form.appendChild(labelextrait)
        form.appendChild(extrait)
        form.appendChild(errorextrait)
        form.appendChild(labelcontenu)
        form.appendChild(contenu)
        form.appendChild(errorcontenu)
        form.appendChild(labelauteur)
        form.appendChild(auteur)
        form.appendChild(errorauteur)
        form.appendChild(labelstatus)
        form.appendChild(status)
        status.appendChild(publish)
        status.appendChild(cache)
        form.appendChild(inputid)
        form.appendChild(submit)
        wrap.append(form)

        validationInput('titre', 2, 100)
        validationInput('extrait', 2, 200)
        validationInput('contenu', 2, 10000)
        validationInput('auteur', 2, 50)

////////////////////////////////
//requête ajax modif articles//
//////////////////////////////
         const modifarticles=document.querySelector('#formModifArticle')
         modifarticles.addEventListener('submit',async (evt) => {
             evt.preventDefault()
             const formdata =new FormData(modifarticles);
             try {
                 const res = await fetch('../ajax/updateArticles.php',{
                     method: 'POST',
                     body: formdata
                 });
                 const json = await res.json();
                 if (json.errors <= 0) {
                     wrap.innerHTML=""
                     getAllArticles()
                     $('html,body').animate({scrollTop: 0});
                 }
            }catch(err){
               console.log(err)
            }
         });
    }catch (err) {
        console.log(err)
    }
}
///////////////////////////
// event creation article//
//////////////////////////
const btnCreatArt=document.createElement('div')
btnCreatArt.classList.add("btn")
btnCreatArt.classList.add("crea")
btnCreatArt.innerText="Ajouter un article"
articlesBlog.append(btnCreatArt)
btnCreatArt.addEventListener("click",(evt)=>{
    if(btnCreatArt.classList.contains('crea')){
        btnCreatArt.innerText="Retour aux avis"
        btnCreatArt.classList.remove('crea')
        h1.innerText="Ajouter un article"
        wrap.innerHTML=""
        creatArticle()
    }else{
        btnCreatArt.classList.add("crea")
        btnCreatArt.innerText="Ajouter un article"
        h1.innerText="Liste et gestion des articles"
        wrap.innerHTML=""
        getAllArticles()
    }
})
//fin event//
/////////////////////
//creation article//
///////////////////
function creatArticle(){
    const form=document.createElement('form')
    form.id="formCreatArticle"
    form.method="POST"
    form.noValidate=true

    const boxChangeImg=document.createElement('div')
    boxChangeImg.classList.add("changeimg")

    const image =document.createElement('input')
    image.type="hidden"
    image.name="image"
    image.id="image"
    const errorimage=document.createElement('span')
    errorimage.classList.add('error')
    errorimage.id="error_image"

    const labelupload=document.createElement('label')
    labelupload.htmlFor="upload"
    labelupload.innerText="uploader une image (png ou jpg)"
    const upload=document.createElement('input')
    upload.type="file"
    upload.name="upload"
    upload.id="upload"
    upload.accept=".png, .jpg"
/////////////////////
//sélection upload//
///////////////////
    upload.addEventListener("change",(evt)=>{
            image.value = upload.files[0].name
    })
//fin event//
    const errorupload=document.createElement('span')
    errorupload.classList.add('error')
    errorupload.id="error_upload"
    console.log(upload.files.length)

    const labeltitre=document.createElement('label')
    labeltitre.htmlFor="titre"
    labeltitre.innerText="Titre de l'article"
    const titre =document.createElement('input')
    titre.type="text"
    titre.name="titre"
    titre.id="titre"
    const errortitre=document.createElement('span')
    errortitre.classList.add('error')
    errortitre.id="error_titre"

    const labelextrait=document.createElement('label')
    labelextrait.htmlFor="extrait"
    labelextrait.innerText="Extrait de l'avis"
    const extrait =document.createElement('input')
    extrait.type="text"
    extrait.name="extrait"
    extrait.id="extrait"
    const errorextrait=document.createElement('span')
    errorextrait.classList.add('error')
    errorextrait.id="error_extrait"

    const labelauteur=document.createElement('label')
    labelauteur.htmlFor="auteur"
    labelauteur.innerText="Auteur de l'article"
    const auteur =document.createElement('input')
    auteur.type="text"
    auteur.name="auteur"
    auteur.id="auteur"
    const errorauteur=document.createElement('span')
    errorauteur.classList.add('error')
    errorauteur.id="error_auteur"

    const labelcontenu=document.createElement('label')
    labelcontenu.htmlFor="contenu"
    labelcontenu.innerText="Contenu de l'article"
    const contenu =document.createElement('textarea')
    contenu.name="contenu"
    contenu.id="contenu"
    const errorcontenu=document.createElement('span')
    errorcontenu.classList.add('error')
    errorcontenu.id="error_contenu"

    const status=document.createElement('input')
    status.name='status'
    status.value="cache"
    status.type="hidden"

    const submit=document.createElement('input')
    submit.type='submit'
    submit.id='submit'
    submit.classList.add('submit')
    submit.name='submitted'

    boxChangeImg.appendChild(labelupload)
    boxChangeImg.appendChild(upload)
    boxChangeImg.appendChild(errorupload)
    form.appendChild(boxChangeImg)
    form.appendChild(labeltitre)
    form.appendChild(titre)
    form.appendChild(errortitre)
    form.appendChild(labelextrait)
    form.appendChild(extrait)
    form.appendChild(errorextrait)
    form.appendChild(labelcontenu)
    form.appendChild(contenu)
    form.appendChild(errorcontenu)
    form.appendChild(labelauteur)
    form.appendChild(auteur)
    form.appendChild(errorauteur)
    form.appendChild(status)
    form.appendChild(submit)
    wrap.append(form)

    validationInput('titre', 2, 100)
    validationInput('extrait', 2, 200)
    validationInput('contenu', 2, 10000)
    validationInput('auteur', 2, 50)

////////////////////////////
//requête ajax ajout avis//
//////////////////////////
    const ajoutArt=document.querySelector('#formCreatArticle')
    ajoutArt.addEventListener('submit',async (evt) => {
        evt.preventDefault()
        const formdata =new FormData(ajoutArt);
        try {
            const res = await fetch('../ajax/insertArticle.php',{
                method: 'POST',
                body: formdata
            });
            const json = await res.json();
            if (json.errors <= 0) {
                btnCreatArt.classList.add("crea")
                btnCreatArt.innerText="Ajouter un article"
                wrap.innerHTML=""
                h1.innerText="Liste et gestion des articles"
                getAllArticles()
                $('html,body').animate({scrollTop: 0});
            }
        }catch(err){
            console.log(err)
        }
    });
}
//////////////////////////////////
//affichage articles par status//
////////////////////////////////
async function getArticlesByStatus(status){
    try{
        const response=await fetch('../ajax/getArticles.php');
        const data=await response.json();

        data.forEach((d) => {
            if (d.status == status) {
                const boxArticle = document.createElement('div')
                boxArticle.classList.add('boxArticle')
                boxArticle.dataset.id = d.id - 1
                console.log(boxArticle.dataset)
                const img = document.createElement("img")
                const lien = `../asset/media/${d.image}`
                img.src = lien
                const titre = d.titre
                img.alt = `${titre}`


                const boxExtraitArticle = document.createElement("div")
                boxExtraitArticle.classList.add("boxExtraitArticle")

                const h3 = document.createElement("h3")
                h3.innerText = titre

                const h4 = document.createElement("h4")
                h4.innerText = `${d.extrait}[...]`

                const auteur = document.createElement("p")
                auteur.classList.add('auteur')
                auteur.innerText = d.auteur

                boxArticle.appendChild(img)
                boxArticle.appendChild(boxExtraitArticle)
                boxExtraitArticle.appendChild(h3)
                boxExtraitArticle.appendChild(h4)
                boxExtraitArticle.appendChild(auteur)
                wrap.appendChild(boxArticle)

//////////////////////////////
//event vers single article//
////////////////////////////
                boxArticle.addEventListener('click',(evt)=>{
                    wrap.innerHTML=""
                    getArticleByClick(boxArticle.dataset.id)
                    $('html,body').animate({scrollTop: 0});
                })
//fin event//
            }
        })
        articlesBlog.append(wrap)
    }catch (err) {
        console.log(err)
    }
}



