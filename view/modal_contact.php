<div class="modal_container" id="modal_contact">
    <div class="modal">
        <span id="closeModalContact"><i class="fa-solid fa-xmark"></i></span>

        <div class="wrap_contact">
            <div class="modal_title">
                <h2>NOUS CONTACTER</h2>
            </div>
            <div class="modal_content">
                <div class="modal_form">
                    <form action="" method="POST" id="contact">
                        <?php if (!empty($_SESSION['user']['nom']) || !empty($_SESSION['user']['prenom']) || !empty($_SESSION['user']['mail']) || !empty($_SESSION['user']['id'])) : ?>
                            <div class="name_form">
                                <div>
                                    <label for="nom">Nom</label>
                                    <input type="text" placeholder="Nom" name="nom_contact" id="nom_contact" value="<?php echo $_SESSION['user']['nom'] ?>" readonly>
                                </div>

                                <div>
                                    <label for="prenom">Prénom</label>
                                    <input type="text" placeholder="Prénom" name="prenom_contact" id="prenom_contact" value="<?php echo $_SESSION['user']['prenom'] ?>" readonly>
                                </div>
                            </div>
                            <div>
                                <label for="email">Email</label>
                                <input type="email" placeholder="Email" name="mail_contact" id="mail_contact" value="<?php echo $_SESSION['user']['mail'] ?>" readonly>
                            </div>
                        <?php else : ?>
                            <div class="name_form">
                                <div>
                                    <label for="nom">Nom</label>
                                    <input type="text" placeholder="Nom" name="nom" id="nom">
                                    <span class="error" id="contact_input_nom"></span>
                                </div>

                                <div>
                                    <label for="prenom">Prénom</label>
                                    <input type="text" placeholder="Prénom" name="prenom" id="prenom">
                                    <span class="error" id="contact_input_prenom"></span>
                                </div>
                            </div>
                            <div>
                                <label for="email">Email</label>
                                <input type="email" placeholder="Email" name="mail" id="mail">
                                <span class="error" id="contact_input_mail"></span>
                            </div>
                        <?php endif; ?>

                        <div class="message_form">
                            <label for="message">Message:</label>
                            <textarea name="message" id="message" placeholder="Message"></textarea>
                            <span class="error" id="contact_input_message"></span>
                        </div>

                        <div class="submit_form">
                            <input type="submit" name="submitted_contact" value="Envoyer">
                        </div>
                    </form>

                </div>

                <div class="carte_reseaux">
                    <div class="map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2594.2931263306778!2d1.098606254098653!3d49.44117659794342!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e0df1548cd768b%3A0x70b4b34959b1ec9f!2sNeed%20for%20School!5e0!3m2!1sfr!2sfr!4v1702638871159!5m2!1sfr!2sfr" width="450" height="250" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                    <div class="social">
                        <h3>Retrouvez nous sur:</h3>
                        <div class="reseaux">
                            <a href=""><i class="fa-brands fa-linkedin-in"></i></a>
                            <a href=""><i class="fa-brands fa-instagram"></i></a>
                            <a href=""><i class="fa-brands fa-dribbble"></i></a>
                            <a href=""><i class="fa-brands fa-x-twitter"></i></a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

