<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Espace client - HippoTrame</title>

    <!-- Custom fonts for this template-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&family=Open+Sans:wght@500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../asset/dist/css/global.bundle.css">
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../asset/dist/css/client.bundle.css">

    <script type="module" src="https://cdn.jsdelivr.net/npm/ldrs/dist/auto/ripples.js"></script>



    <?php
    if (!isLogged()) {
        header("Location: ../index.php");
        exit;
    }
    ?>
    <?php if (isWebpack('gestionAvis', true, 'client')) { ?>
        <link rel="stylesheet" href="../asset/dist/css/gestionavis.bundle.css">
    <?php } ?>

    <?php if (isWebpack('gestionBlog', true, 'client')) { ?>
        <link rel="stylesheet" href="../asset/dist/css/gestionblog.bundle.css">
    <?php } ?>

    <?php if (isWebpack('gestionUser', true, 'client')) { ?>
        <link rel="stylesheet" href="../asset/dist/css/gestionuser.bundle.css">
    <?php } ?>


</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!--        <i class="fa-solid fa-hippo"></i>-->

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="../index.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    <img src="../asset/img/HippotrameV1.png" alt="" class="hippo-logo">
                </div>
                <div class="sidebar-brand-text mx-3">Hippotrame</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">

                <a class="nav-link" href="index.php">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Mon analyse réseau</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Mon espace personnel
            </div>

            <li class="nav-item">
                <a class="nav-link" href="../profil.php">
                    <i class="fa-solid fa-user-pen"></i>
                    <span>Modifier profil</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">


            <!-- Heading -->
            <div class="sidebar-heading">
                Informations
            </div>

            <li class="nav-item">
                <a class="nav-link" href="../blog.php">
                    <i class="fa-solid fa-newspaper"></i>
                    <span>Blog</span></a>
            </li>


            <li class="nav-item">
                <a class="nav-link" href="../services.php">
                    <i class="fa-solid fa-circle-exclamation"></i>
                    <span>Guide</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">
            <!-- //////////////////////////////////////// -->

            <?php if (isAdmin()) : ?>
                <!-- Heading -->
                <div class="sidebar-heading">
                    Gestion admin
                </div>

                <!-- Nav Item - Pages Collapse Menu -->
                <li class="nav-item">
                    <a class="nav-link" href="gestionUser.php">
                        <i class="fa-solid fa-user-pen"></i>
                        <span>Gestion : utilisateur</span></a>
                </li>

                <!-- Nav Item - Charts -->
                <li class="nav-item">
                    <a class="nav-link" href="gestionBlog.php">
                        <i class="fas fa-fw fa-wrench"></i>
                        <span>Gestion : blog</span></a>
                </li>

                <!-- Nav Item - Tables -->
                <li class="nav-item">
                    <a class="nav-link" href="gestionAvis.php">
                        <i class="fas fa-fw fa-wrench"></i>
                        <span>Gestion : avis</span></a>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider d-none d-md-block">


            <?php endif; ?>
            <!-- //////////////////////////////////////// -->


            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

            <!-- Sidebar Message -->
            <div class="premium-box">
            <div class="sidebar-card d-none d-lg-flex">
                <img src="../asset/img/hippotramePro.png" alt="" class="hippo-pro">
                <p class="text-center mb-2"><strong>Hippotrame +</strong> est pourvu de fonctionnalités premium, d'une analyse réseau toujours plus fournie, et plus encore !</p>
                <a class="btn btn-success btn-sm btn-premium premium-link-2" href="">Devenir un pro !</a>
            </div>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo ucfirst($_SESSION['user']['nom'])  . " " . ucfirst($_SESSION['user']['prenom']) ?></span>
                                <img class="img-profile rounded-circle" src="img/undraw_profile.svg">
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="../profil.php">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Profil
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Déconnexion
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->