<section id="services">
    <div class="wrap">
        <div class="services_title">
            <h3>Comment ça marche?</h3>
        </div>

        <div class="services_container">
            <div class="services_left">
                <img src="asset/img/howto.png" alt="">
            </div>
            <div class="services_right">
                <div class="services_card">
                    <div class="card">
                        <h3>1. Inscription</h3>
                        <p>Tout commence par l'inscription à nos services, permettant ainsi d'accéder à vos analyses à tout moment.</p>
                    </div>
                    <div class="card">
                        <h3>2. Connexion</h3>
                        <p>Ensuite, il vous suffit de vous connecter en utilisant vos identifiants.</p>
                    </div>
                    <div class="card">
                        <h3>3. Mon Espace</h3>
                        <p>Enfin, rendez-vous dans la section "Mon Espace" pour analyser vos trames et obtenir des informations approfondies sur vos réseaux.</p>
                    </div>
                </div>
                <div class="services_info">
                    <p>Enjoy!</p>
                </div>
            </div>
        </div>
    </div>
</section>