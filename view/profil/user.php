<div id="profil">
    <div class="wrap">
        <div class="profil_title">
            <h3>PROFIL</h3>
        </div>
        <div class="profil_content">
            <div class="profil_form">
                <div>
                    <h3>Nom:</h3>
                    <span><?php echo ucfirst($_SESSION['user']['nom']) ?></span>
                </div>
                <div>
                    <h3>Prénom:</h3>
                    <span><?php echo ucfirst($_SESSION['user']['prenom']) ?></span>
                </div>
                <div>
                    <h3>E-mail:</h3>
                    <span><?php echo ucfirst($_SESSION['user']['mail']) ?></span>
                </div>
            </div>
            <div class="profil_image">
                <img src="asset/img/user_image.png" alt="">
            </div>
        </div>
    </div>
</div>