<section id="fonctionnement">
    <div class="wrap">
        <div class="fonctionnement_logo">
            <div class="fonctionnement_logo_box">
                <img src="asset/img/logo.png" alt="Logo HippoTrame" />
            </div>
        </div>
        <div class="fonctionnement_text ">
            <div class="flexslider" id="how">
                <ul class="slides">
                    <li>
                        <div class="slide_text">
                            <div>
                                <h3>COMMENT ça MARCHE ?</h3>
                                <p>Explorez le fonctionnement de nos services avec une explication concise et découvrez notre expertise à travers les différentes étapes de notre processus.</p>
                            </div>
                            <a href="services.php">EN SAVOIR PLUS</a>
                        </div>
                    </li>
                    <li>
                        <div class="slide_text">
                            <div>
                                <h3>Les actualités ?</h3>
                                <p>Restez à jour grâce à notre section actualités, une source régulièrement mise à jour pour vous tenir informé des derniers événements et tendances dans notre domaine.</p>
                            </div>
                            <a href="blog.php">DECOUVRIR</a>
                        </div>
                    </li>
                    <li>
                        <div class="slide_text">
                            <div>
                                <h3>Notre équipe ?</h3>
                                <p>Explorez et plongez dans l'univers de notre remarquable équipe de développeurs, regorgeant de compétences variées et d'expertise passionnée.</p>
                            </div>
                            <a href="#team">DECOUVRIR</a>
                        </div>
                    </li>
                   
                </ul>
            </div>
        </div>

    </div>
</section>