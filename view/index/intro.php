<section id="intro">
    <div class="wrap">
        <div class="intro_text">
            <h1>HippoTrame</h1>
            <h2>Votre expert en analyse réseau !</h2>
            <a href="/client/">COMMENCER</a>
        </div>
        <div class="intro_img">
            <img src="asset/img/intro_loupe.png" alt="loupe"/>
        </div>
    </div>
</section>
