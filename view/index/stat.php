<section id="stat">
    <div class="wrap">
        <div class="stat_number">
            <div>
                <h2>100%</h2>
                <p>de nos clients Satisfait</p>
            </div>
            <div>
                <h2>1000</h2>
                <p>utilisateurs nous ont déjà fait confiance</p>
            </div>
            <div>
                <h2>5 <i class="fa-regular fa-star"></i></h2>
                <p>SUR Trustpilot</p>
            </div>
        </div>
        <div class="stat_button">
            <a href="/client/">COMMENCER</a>
        </div>
    </div>
</section>