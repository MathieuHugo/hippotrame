<section id="team">
    <div class="wrap">
        <div class="team_title">
            <h3>Notre équipe</h3>
        </div>
        <div class="team_card">
            <div class="team_card_single">
                <img src="asset/img/profil1.png" alt="">
                <div class="team_card_text">
                    <span>Bilel</span>
                    <p>Chef de projet / Développeur FullStack</p>
                </div>
            </div>
            <div class="team_card_single">
                <img src="asset/img/profil2.png" alt="">
                <div class="team_card_text">
                    <span>Kévin</span>
                    <p>Développeur FullStack</p>
                </div>
            </div>
            <div class="team_card_single">
                <img src="asset/img/profil3.png" alt="">
                <div class="team_card_text">
                    <span>Hugo</span>
                    <p>Développeur FullStack</p>
                </div>
            </div>
            <div class="team_card_single">
                <img src="asset/img/profil4.png" alt="">
                <div class="team_card_text">
                    <span>Enzo</span>
                    <p>Designer <br>web</p>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="loading-screen">
    <h1>HippoTrame</h1>
    <l-ripples size="400" speed="7" color="#00000099"></l-ripples>
</div>