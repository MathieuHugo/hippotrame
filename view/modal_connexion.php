<div class="modal_container" id="modal_connexion">
    <div class="modal">
        <span id="closeModalConnexion"><i class="fa-solid fa-xmark"></i></span>

        <div class="wrap_modal">
            <div class="modal_title">
                <h2>CONNEXION</h2>
            </div>
            <div class="modal_form">
                <form id="connexion" action="" method="POST" novalidate>

                    <div>
                        <label for="email">Email</label>
                        <input type="email" name="mail" id="mail_connex" placeholder="Email" autocomplete="username">
                        <span class="error" id="connex_input_mail"></span>
                    </div>

                    <div>
                        <label for="password">Mot de Passe</label>
                        <input type="password" name="password" id="password_connex" placeholder="Mot de Passe" autocomplete="current-password">
                        <span class="error" id="connex_input_password"></span>
                    </div>

                    <div class="submit_form">
                        <div>
                            <a href="">Mot de passe oublié?</a>
                            <a href="" id="connexToInsc">Pas encore de compte?</a>
                        </div>
                        <div>
                            <input type="submit" name="submitted_connexion" value="Se Connecter">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
