<footer>
    <div class="wrap">
        <a href="mention.php">Mentions légales</a>
        <ul>
            <li class="fleche"></li>
            <li>
                <p>Copyright&copy; 2023 Hippotrame All Rights Reserved.</p>
            </li>
        </ul>

        <ul>
            <li><a href=""><i class="fa-brands fa-linkedin-in"></i></a></li>
            <li><a href=""><i class="fa-brands fa-instagram"></i></a></li>
            <li><a href=""><i class="fa-brands fa-dribbble"></i></a></li>
            <li><a href=""><i class="fa-brands fa-twitter"></i></a></li>
        </ul>
    </div>
</footer>

<?php
include('../view/modal_inscription.php');
include('../view/modal_connexion.php');
include('../view/modal_contact.php');
?>


<script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
<script src="./asset/flexslider/jquery.flexslider-min.js"></script>
<script src="./asset/dist/js/global.bundle.js"></script>



<?php if (isWebpack('blog')) { ?>
    <script src="./asset/dist/js/blog.bundle.js"></script>
<?php }
if (isWebpack('index')) { ?>
    <script src="./asset/dist/js/index.bundle.js"></script>
<?php }
if (isWebpack('index')) { ?>
    <script src="./asset/dist/js/main.bundle.js"></script>
<?php } ?>

<?php if (isWebpack('mention')) { ?>
    <script src="./asset/dist/js/mention.bundle.js"></script>
<?php } ?>

<?php if (isWebpack('services')) { ?>
    <script src="./asset/dist/js/services.bundle.js"></script>
<?php } ?>

<?php if (isWebpack('profil')) { ?>
    <script src="./asset/dist/js/profil.bundle.js"></script>
<?php } ?>


<script src="./asset/dist/js/inscription.bundle.js"></script>
<script src="./asset/dist/js/connexion.bundle.js"></script>




</body>

</html>