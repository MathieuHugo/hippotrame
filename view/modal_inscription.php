<div class="modal_container" id="modal_inscription">
    <div class="modal">
        <span id="closeModalInscription"><i class="fa-solid fa-xmark"></i></span>
        <div class="wrap_modal">
            <div class="modal_title">
                <h3>INSCRIPTION</h3>
                <div class="return_reg">Inscription réussi</div>
            </div>
            <div class="modal_form">
                <form id="inscription" action="" method="POST" novalidate>
                    <div class="name_form">
                        <div>
                            <label for="nom">Nom</label>
                            <input type="text" name="nom" id="nom" placeholder="Nom">
                            <span class="error" id="input_nom"></span>
                        </div>

                        <div>
                            <label for="prenom">Prénom</label>
                            <input type="text" name="prenom" id="prenom" placeholder="Prénom">
                            <span class="error" id="input_prenom"></span>
                        </div>
                    </div>

                    <div class="mail_form">
                        <label for="mail">Email</label>
                        <input type="email" name="mail" id="mail" placeholder="Email" autocomplete="username">
                        <span class="error" id="input_mail"></span>
                    </div>

                    <div class="password_form">
                        <div>
                            <label for="password">Mot de Passe</label>
                            <input type="password" name="password" id="password" placeholder="Mot de Passe" autocomplete="new-password">
                            <span class="error" id="input_password"></span>
                        </div>
                        <div>
                            <label for="password_confirm">Confirmer le Mot De Passe</label>
                            <input type="password" name="password2" id="password2" placeholder="Confirmer le Mot De Passe" autocomplete="new-password">
                        </div>

                    </div>
                    <div class="submit_form">
                        <div>
                            <a href="" id="inscToConnex" >Vous avez deja un compte?</a>
                        </div>
                        <div>
                            <input type="submit" name="submitted" value="S’Inscrire">
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>