<!doctype html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="./asset/img/hippotrame-favicon.png" type="image/x-icon">
    <title>Hippotrame</title>
    <link rel="stylesheet" href="./asset/flexslider/flexslider.css" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&family=Open+Sans:wght@500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./asset/dist/css/global.bundle.css">
    <script type="module" src="https://cdn.jsdelivr.net/npm/ldrs/dist/auto/ripples.js"></script>



    <?php if (isWebpack('contact')) { ?>
        <link rel="stylesheet" href="./asset/dist/css/contact.bundle.css">
    <?php } ?>

    <?php if (isWebpack('index')) { ?>
        <link rel="stylesheet" href="./asset/dist/css/main.bundle.css">
    <?php } ?>

    <?php if (isWebpack('mention')) { ?>
        <link rel="stylesheet" href="./asset/dist/css/mention.bundle.css">
    <?php } ?>

    <?php if (isWebpack('blog')) { ?>
        <link rel="stylesheet" href="../asset/dist/css/blog.bundle.css">
    <?php } ?>

    <?php if (isWebpack('services')) { ?>
        <link rel="stylesheet" href="./asset/dist/css/services.bundle.css">
    <?php } ?>

</head>
<header>
    <div class="wrap">
        <a href="index.php"><img src="./asset/img/logo-hippo.png" alt="logo hippotrame" class="logo"></a>
        <nav>
            <ul>
                <li><a href="blog.php">Actualités</a></li>

                <li><a href="services.php">Comment ça marche?</a></li>
                <li><a id="button_contact" href="">Contact</a></li>

                <ul class="log">

                    <?php if (isLogged()) : ?>
                        <li><a id="profil" href="/client/">Mon Espace</a></li>
                        <li><a id="deconnexion" href="logout.php">Déconnexion</a></li>
                    <?php else : ?>
                        <li><a id="button_inscription" href="">Inscription</a></li>
                        <li><a id="button_connexion" href="">Connexion</a></li>
                    <?php endif; ?>
                </ul>
            </ul>
        </nav>
    </div>
</header>

<body>
