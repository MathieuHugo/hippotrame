-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mar. 19 déc. 2023 à 16:16
-- Version du serveur : 10.4.28-MariaDB
-- Version de PHP : 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `hippotrame`
--

-- --------------------------------------------------------

--
-- Structure de la table `avis`
--

CREATE TABLE `avis` (
  `id` int(11) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `title` varchar(30) NOT NULL,
  `job` varchar(30) NOT NULL,
  `message` text NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime DEFAULT NULL,
  `etat` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `avis`
--

INSERT INTO `avis` (`id`, `nom`, `prenom`, `title`, `job`, `message`, `created_at`, `modified_at`, `etat`) VALUES
(1, 'QUENE', 'Kevin', 'Génial', 'Développeur WEB', 'Super application WEB, fonctionnelle et ergonomique.', '2023-12-17 14:23:46', '2023-12-19 14:47:39', 'publish'),
(2, 'MATHIEU', 'Hugo', 'Incroyable', 'Développeur WEB', 'Design épuré, interface ergonomique!\r\n', '2023-12-17 14:24:54', NULL, 'publish'),
(3, 'DUFEU', 'Enzo', 'efficace', 'Développeur WEB', 'Une analyse réseau claire, et facile à prendre en main.', '2023-12-17 14:25:58', '2023-12-19 12:41:13', 'publish'),
(4, 'DOUNAR', 'Billel', 'C\'est ok', 'Chef de projet', 'En plus d\'avoir un design atypique comparé aux autre appli WEB, on a entre les mains une appli complète.', '2023-12-17 14:27:55', '2023-12-19 14:54:18', 'publish'),
(5, 'QUENE', 'Kevin', 'Génial', 'Développeur web', 'Site magnifique, design épuré, ergonomique et efficace dans son analyse.', '2023-12-19 14:52:04', NULL, 'publish'),
(6, 'QUENE', 'Kevin', 'Incroyable', 'Développeur web', 'En plus d\'être fonctionnel, il est magnifique.\r\nUn rayon de soleil dans le domaine.', '2023-12-19 14:56:29', NULL, 'publish');

-- --------------------------------------------------------

--
-- Structure de la table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `titre` varchar(100) NOT NULL,
  `auteur` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `extrait` varchar(200) NOT NULL,
  `image` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime DEFAULT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `blog`
--

INSERT INTO `blog` (`id`, `titre`, `auteur`, `content`, `extrait`, `image`, `created_at`, `modified_at`, `status`) VALUES
(1, 'Une faille critique dans Apache Struts activement exploitée', 'Dominique Filippone', 'Danger pour les utilisateurs du framework open source Apache Struts pour rationaliser le développement d\'applications web Java EE. Très répandu au sein des entreprises, mais aussi des administrations, ce dernier a été affecté par une vulnérabilité identifiée comme critique (CVE-2023-50164). La faille RCE (exécution de code à distance) affecte les versions Struts 2.0.0 à 2.3.37 (fin de vie), 2.5.0 à 2.5.32 et 6.0.0 à 6.3.0. En réaction, la fondation Apache qui gère ce projet a publié des mises à jour 6.3.0.2 et 2.5.33 pour résoudre ce sérieux problème de sécurité. Si toutes les conditions d\'exploit sont réunies, un cybercriminel peut télécharger des fichiers malveillants et réaliser une exécution de code à distance sur un serveur cible. Avec à la clé modifier des fichiers sensibles, voler des données, perturber des services critiques ou réaliser des déplacements latéraux dans le réseau.\r\n\r\nCette vulnérabilité a été identifiée par Apache le 7 décembre avec donc des mises à jour poussées dans la foulée à appliquer dès que possible. Le timing d\'application de ces MAJ s\'est affolé lorsqu\'un PoC d\'exploit a été publié par un chercheur en sécurité le 10 décembre, laissant donc toute une belle opportunité à des pirates d\'affuter leur exploit sur des systèmes non mis à jour. Et ce qui devait arriver arriva : selon la plateforme d\'analyse des menaces Shadowserver, des pirates auraient commencé à se mettre au travail, des chercheurs ayant observé un petit nombre d\'adresses IP engagées dans des tentatives d\'exploitation. ', 'La vulnérabilité CVE-2023-50164 considérée critique dans Apache Struts débouche sur de l\'injection de malware et de l\'exécution de code à distance. ', 'imgBlog0.png', '2023-12-19 16:07:12', NULL, 'publish'),
(2, 'Commutateurs Ethernet : hausse de 16 % du marché mondial au 3e trimestre 2023', 'Fabrice Alessi ', 'Le marché mondial des commutateurs Ethernet continue de surfer sur une dynamique de croissance forte. Selon IDC, au cours du troisième trimestre 2023, les ventes de ces équipements réseaux ont grimpé de 15,8 % d\'une année sur l\'autre et généré 11,7 Md$ de chiffre d\'affaires dans le monde. Sur neuf mois, leur progression s\'affiche ainsi à 27,7 %, contre 18,7 % sur l\'ensemble de l\'année 2022. Le cabinet d\'études explique que l\'un des principaux moteurs de la croissance du marché reste l\'atténuation des problèmes de la chaîne d\'approvisionnement. Grâce à l\'amélioration de la disponibilité des composants, les fournisseurs sont de plus en plus en mesure de comptabiliser les recettes provenant des livraisons de commandes de produits en attente.\r\n\r\n92 % de croissance sur le segment 2.5/5GbE\r\n\r\nCette amélioration de la situation produit ses effets le plus visibles sur le segment des commutateurs Ethernet qui ne sont pas destinés à équiper les data centers. Son chiffre d\'affaires trimestriel a connu une hausse de 22,2 % (+36,5 % sur neuf mois). En comparaison, les ventes de commutateurs pour data centers ont une croissance bien plus modeste de 7,2 % (+16,8 % sur neuf mois). Dans cette catégorie, les hyperscalers et les fournisseurs de services cloud continuent de favoriser l\'adoption de produits toujours plus rapide. Résultats : les revenus des livraisons de commutateurs 200/400 GbE ont augmenté de 44,0 % entre juillet et septembre derniers. Quant aux ventes d\'équipements n\'ayant vocation à être installés dans des data centers, elles tirent leur croissance globale notamment de la bonne tenue des segments 1GbE (+18,3 %) et 2.5/5GbE (+ 92 %).\r\n\r\nHPE explose les compteurs\r\n\r\nHPE a tiré pleinement partie de son positionnement sur le marché des commutateurs « non-data centers » (92 % de ses revenus dans la commutation). Au cours du troisième trimestre, ses ventes ont décollé de 88,4 %, lui apportant ainsi 7,7 % de parts de marché. Privilégiant de son côté les commutateurs Ethernet pour data center, Arista Networks affiche tout de même une belle performance, soit des ventes en croissance de 27,3 % pour 10,6 % de part de marché. Devant lui, Cisco trône à la place de numéro un mondial (45,1 % de part de marché) et affiche des revenus en progression de 20,1 %. La troisième place du classement est occupée par Huawei qui n\'a pas su profiter de la hausse du marché mondial de la commutation Ethernet. Le chinois a perdu 1 % de son chiffre d\'affaires pour une part de marché de 9,6 %.', 'Porté par les livraisons d\'équipements n\'étant pas destinés à équiper les data centers, le marché mondial des commutateurs Ethernet a représenté 11,7 Md$ au troisième trimestre 2023 selon IDC.', 'imgBlog1.png', '2023-12-14 18:49:40', NULL, 'publish'),
(3, 'IBM dégaine un service multicloud mesh sécurisé', 'Michael Cooney', 'Baptisé Hybrid Cloud Mesh, ce service tout récemment annoncé par IBM fournit un environnement de réseau virtualisé qui facilite la mise en place d’une connectivité sécurisée entre utilisateurs, applications et données distribuées à travers de multiples environnements edge, hybrides et multicloud. « L\'une des raisons d\'être de ce service cloud est sa capacité à unifier les silos opérationnels, en donnant aux équipes IT un contrôle granulaire du réseau et des interfaces faciles à utiliser », a déclaré IBM. « Cloud Mesh permet aux entreprises d\'établir une connectivité simple, évolutive et sécurisée, centrée sur les applications », a écrit Murali Gandluru, vice-président d\'IBM Software Networking and Edge, dans un blog sur l’offre Hybrid Cloud Mesh. « Le produit apporte aussi une prévisibilité en termes de latence, de bande passante et de coût. Il permet aux équipes CloudOps et DevOps de gérer et de mettre à l\'échelle de manière transparente les applications réseau, y compris les applications cloud-natives fonctionnant sur Red Hat OpenShift », a déclaré Murali Gandluru.\r\n\r\n« Hybrid Cloud Mesh fonctionne en déployant des passerelles dans les clouds - sur site, AWS ou les clouds d\'autres fournisseurs, et les points de transit, si nécessaire - de façon à prendre en charge l\'infrastructure, puis il construit des couches 3 à 7 maillées sécurisées pour fournir des applications », a expliqué IBM. « Le service met en œuvre deux types de passerelles : une passerelle edge, déployée à proximité des charges de travail pour le transfert, l\'application de la sécurité, l\'équilibrage de charge et la collecte de données télémétriques ; et/ou une passerelle de point de passage ou Waypoint Gateway, déployée aux points de présence près des échanges Internet et des sites de colocation pour l\'optimisation du chemin, du coût et de la topologie », a déclaré Murali Gandluru. « Au niveau de l\'application, les développeurs sont exposés à la couche 7, et les équipes de mise en réseau voient les activités des couches 3 et 4 », a indiqué IBM.', 'Le service réseau multicloud IBM Hybrid Cloud Mesh comprend des fonctions de découverte, de sécurité, de surveillance et d\'ingénierie du trafic.', 'imgBlog2.png', '2023-12-14 18:54:21', NULL, 'publish'),
(4, 'Une faille bluetooth expose des millons de systèmes Android, Linux et MacOS/iOS', 'Dominique Filippone', 'De BLURtooth à BrakTooth, les vulnérabilités affectant le bluetooth - et par effet domino les terminaux qui disposent de cette fonction - sont légion. Citons également un peu plus loin dans temps (2016) MouseJack qui affectait dongles et souris de 17 fournisseurs rendant possible la prise de contrôle d\'un système via de l\'injection de touches (keystroke injection). A l\'origine de la découverte de cette dernière faille, un chercheur en sécurité baptisé Keyboard qui vient de se rappeler à notre bon souvenir en publiant une dernière recherche révélant l\'existence d\'une nouvelle faille bluetooth. \r\n\r\n« Plusieurs piles bluetooth présentent des vulnérabilités de contournement d\'authentification qui permettent à un attaquant de se connecter à un hôte découvrable sans confirmation de l\'utilisateur et effectuer de l\'injection de touches », explique le chercheur en sécurité. « Un attaquant situé à proximité peut se connecter à un appareil vulnérable via bluetooth non authentifié et injecter des séquences de touches pour, par exemple, installer des applications, exécuter des commandes arbitraires, transférer des messages, etc. ». Le chercheur précise par ailleurs que l\'exploit de cette faille (CVE-2023-45866) ne nécessite pas de matériel spécialisé et peut être réalisée à partir d\'un ordinateur Linux utilisant un adaptateur bluetooth normal. ', 'Des millions de terminaux sont vulnérables à une faille dans bluetooth entrainant un contournement d\'authentification et une injection de frappes. ', 'imgBlog3.png', '2023-12-14 18:56:56', NULL, 'publish'),
(5, 'Télécom SudParis ouvre ses formations en cybersécurité et réseaux à l\'apprentissage', 'Véronique Arène ', 'Comme annoncé en décembre 2022, Télécom SudParis rend  ses deux mastères spécialisés en cybersécurité et réseaux & services accessibles par la voie de l’apprentissage à partir de septembre 2024. Les étudiants en poursuite d’études, salariés en poste ou professionnels en reconversion pourront opter pour deux formules, en contrat d’apprentissage ou de professionnalisation, selon leur âge et/ou leur situation professionnelle.  D’un côté, la spécialité de cybersécurité prépare aux métiers d’experts de la sécurité des infrastructures informatiques et des données en abordant les problématiques et les enjeux actuels et futurs du domaine. Différents aspects de la cybersécurité sont passés en revue :  de l\'organisation jusqu’à l\'audit, en passant par la réglementation et les techniques de sécurisation des systèmes d’information et des réseaux.\r\n\r\nA noter des interventions de l’ANSSI pour préparer les étudiants à répondre à toutes les obligations imposées aux entreprises, administrations et états par la récente réglementation cyber européenne (directives NIS et NIS 2). Les participants sont en formation trois jours par semaine sur le campus de Palaiseau (Essonne) de septembre à mars. De l’autre, le mastère spécialisé en réseaux et services associés fournit un aperçu des dernières technologies réseau et approfondit les expertises en matière de services associés. Un bloc de formation dédié notamment à la fibre optique et aux réseaux cœur et accès a été intégré au parcours en réponse à l’évolution considérable ces dernières années des systèmes d’information et des services connectés.\r\n\r\nDes opportunités de stages déjà ouvertes\r\nDes interventions de professionnels chevronnés livrant leurs expertises du terrain conduiront à des mises en pratique des méthodes et techniques. Le rythme d\'alternance se compose d’une semaine de formation sur le campus d’Evry suivie de trois semaines en entreprise, pendant une année scolaire.  Accréditées par la Conférence des Grandes Ecoles (CGE), ces deux disciplines conduisent à l’obtention de titres de niveau 7 (équivalent à Bac+6) inscrits au Répertoire National des Certifications Professionnelles (RNCP) de France compétences. D’ores et déjà, plusieurs entreprises partenaires de Télécom SudParis ont ouvert des contrats d’alternance pour des postes d’ingénieur ou de consultant à destination des futurs participants de ces mastères.\r\n\r\nLes candidatures - diplômés bac +5 ou bac +3 / +4 ayant une expérience professionnelle pertinente dans ces domaines -   pour la session de septembre 2024 sont d’ores et déjà ouvertes et seront clôturées fin juin 2024. Des jurys d\'admission se réuniront chaque mois à partir de janvier pour évaluer et sélectionner les candidats qui intègreront les programmes. Pour ceux qui souhaitent en savoir plus, des journées portes ouvertes auront lieu le samedi 9 décembre 2023 à partir de 13h 00 sur le campus d\'Evry et en live sur YouTube. ', 'Fidèle à ses engagements pris l\'an dernier, l\'école publique d\'ingénieurs Télécom SudParis fait basculer ses deux mastères spécialisés en sécurité informatique et en réseaux et services associés.', 'imgBlog4.png', '2023-12-14 18:58:23', NULL, 'cache'),
(6, 'Le SIAAP touché par une virulente cyberattaque', 'Dominique Filippone ', 'Spécialisé dans la dépollution des eaux usées en Ile de France, le SIAAP (Service public de l\'assainissement francilien) traite quotidiennement 50 000m3 d’eaux usées. Cet organisme, dont l\'activité est particulièrement critique, a été touché par une cyberattaque vendredi qui s\'est avérée particulièrement virulente. « Ces dernières quarante-huit heures, l’action des équipes spécialisées du SIAAP s’est principalement concentrée sur l’objectif de sécurisation maximale de l’informatique industrielle permettant d’assurer le pilotage de l’activité des réseaux et des usines », a indiqué l\'établissement dans un communiqué publié ce samedi. \r\n\r\nContacté pour des précisions, un porte-parole du groupe nous a à ce stade indiqué que cette attaque informatique ne concerne pas les systèmes industriels du SIAAP mais « seulement » l’informatique de gestion (messagerie, ressources humaines…). « Les activités opérationnelles d’assainissement (transport et épuration) ne sont pas concernées et restent assurées », nous a précisé cette source. « Au regard de cette attaque visiblement très structurée et de ses conséquences sur le fonctionnement du SIAAP, une plainte a été déposée auprès des services de la police judiciaire ainsi qu’une déclaration auprès de la Commission nationale de l’informatique et des libertés », a également fait savoir le groupe.\r\n\r\nTrop tôt pour un retour à la normale \r\nLe SIAAP a également déclenché un plan d\'action pour neutraliser tous les vecteurs de connexion informatique et numérique externes afin d’empêcher au maximum la propagation depuis et vers l’extérieur de l’attaque repérée en fin de semaine.\r\n\r\nLe retour à la normale de l\'activité devrait prendre plusieurs semaines.  « La cellule de crise du SIAAP reste mobilisée pour gérer les suites de cette attaque et accompagner dès cette semaine la continuité du travail de l’ensemble de ses agents dans un contexte et un environnement de travail largement dégradés par la situation actuelle ».', 'Le service public de l\'assainissement francilien a annoncé avoir été victime d\'une attaque informatique qualifiée de très structurée. Des mesures ont été prises pour circonscrire la propagation.', 'imgBlog5.png', '2023-12-14 19:04:17', NULL, 'cache');

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nom` varchar(25) NOT NULL,
  `prenom` varchar(25) NOT NULL,
  `objet` varchar(40) NOT NULL,
  `message` text NOT NULL,
  `answer_content` text NOT NULL,
  `answer_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `categorie` varchar(25) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `mail` varchar(80) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime DEFAULT NULL,
  `token` varchar(255) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `avis`
--
ALTER TABLE `avis`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `avis`
--
ALTER TABLE `avis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
